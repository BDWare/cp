# 项目说明
## DebugRun说明
1.复制debugconf.json.template
```
copy debugconf.json.template debugconf.json
```
2.修改debugconf.json
```
{
  "agentHttpAddr": "127.0.0.1:18000",
  "script": "xxx/xx/xxxx.ypk", //ypk路径
  "pubKey": "04d1xxx", //在agent有权限的pubkey
  "privKey": "df8" //pubkey对应的privkey
}
```
3.设置DebugMain的路径为：xxx/xxx/cp
```
执行DebugMain.main即可
```