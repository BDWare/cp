package org.bdware.sc.boundry;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ScriptReturnException extends IllegalStateException {
    public JsonObject message;

    public ScriptReturnException(JsonElement jsonElement) {
        message = jsonElement.getAsJsonObject();
    }
}
