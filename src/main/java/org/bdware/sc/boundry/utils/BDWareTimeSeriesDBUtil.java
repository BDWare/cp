package org.bdware.sc.boundry.utils;

import org.bdware.sc.ContractProcess;
import org.bdware.sc.compiler.PermissionStub;
import org.bdware.sc.db.TimeRocksDBUtil;
import org.bdware.sc.node.Permission;

import java.io.File;

@PermissionStub(permission = Permission.BDWareTimeSeriesDB)
public class BDWareTimeSeriesDBUtil {
    public static TimeRocksDBUtil getConnection() {
        File parent = new File("./ContractDB/" + ContractProcess.instance.getContractName());
        parent = new File(parent, "BDWareTimeSeriesDB");
        return new TimeRocksDBUtil(parent.getAbsolutePath());
    }

    public static TimeRocksDBUtil getConnection(String dbName) {
        File parent = new File("./ContractDB/" + ContractProcess.instance.getContractName());
        parent = new File(parent, dbName);
        return new TimeRocksDBUtil(parent.getAbsolutePath());
    }
}
