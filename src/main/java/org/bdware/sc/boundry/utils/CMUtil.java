package org.bdware.sc.boundry.utils;

import org.bdware.sc.boundry.JavaScriptEntry;
import org.bdware.sc.compiler.PermissionStub;
import org.bdware.sc.node.Permission;

@PermissionStub(permission = Permission.CM)
public class CMUtil {
    public static String getTimesOfExecution(String contractName) {
        return JavaScriptEntry.get.syncGet("", "getTimesOfExecution", contractName);
    }
}
