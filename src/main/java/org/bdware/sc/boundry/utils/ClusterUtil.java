package org.bdware.sc.boundry.utils;

import org.bdware.sc.boundry.JavaScriptEntry;
import org.bdware.sc.compiler.PermissionStub;
import org.bdware.sc.node.Permission;
import wrp.jdk.nashorn.internal.objects.NativeArray;

@PermissionStub(permission = Permission.Cluster)
public class ClusterUtil {
    public static int getShardingID() {
        return JavaScriptEntry.shardingID;
    }

    public static Object getMembers() {
        if (JavaScriptEntry.members == null) {
            return new NativeArray();
        }
        NativeArray narray = new NativeArray();
        for (int i = 0; i < JavaScriptEntry.members.size(); i++)
            NativeArray.push(narray, JavaScriptEntry.members.get(i));
        return narray;
    }

    public static String getMember(int i) {
        return JavaScriptEntry.members.get(i);
    }

    public static int getMembersSize() {
        if (JavaScriptEntry.members != null)
            return JavaScriptEntry.members.size();
        return 1;
    }

    public static Object getCurrentNodeID() {
        if (isCluster()) {
            return JavaScriptEntry.members.get(JavaScriptEntry.shardingID);
        }
        return null;
    }

    public static boolean isCluster() {
        return (JavaScriptEntry.members != null);
    }
}
