package org.bdware.sc.boundry.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class DOMUtil {
    public static Document parse(String html) {
        return Jsoup.parse(html);
    }
}
