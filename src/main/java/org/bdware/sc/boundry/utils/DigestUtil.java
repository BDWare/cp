package org.bdware.sc.boundry.utils;

import org.bdware.sc.compiler.PermissionStub;
import org.bdware.sc.node.Permission;

@PermissionStub(permission = Permission.Digest)
public class DigestUtil {
    public static String md5Hex(String arg) {
        return org.apache.commons.codec.digest.DigestUtils.md5Hex(arg);
    }

    public static String sha256Hex(String arg) {
        return org.apache.commons.codec.digest.DigestUtils.sha256Hex(arg);
    }
}
