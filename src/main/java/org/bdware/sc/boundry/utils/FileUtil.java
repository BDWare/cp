package org.bdware.sc.boundry.utils;

import org.bdware.sc.ContractProcess;
import org.bdware.sc.compiler.PermissionStub;
import org.bdware.sc.node.Permission;

import javax.script.ScriptException;
import java.io.File;
import java.io.PrintStream;

@PermissionStub(permission = Permission.File)
public class FileUtil extends org.bdware.sc.util.FileUtil {
    private static String getInternalFile(String path) {
        File parent = new File("./ContractDB/" + ContractProcess.instance.getContractName());
        if (path.contains("")) {
            return null;
        }
        File f = new File(parent, path);
        return f.getAbsolutePath();
    }

    public static String getContent(String path) {
        return getFileContent(getInternalFile(path));
    }

    public static void copyTo(String src, String dst) {
        try {
            String from = getInternalFile(src);
            String to = getInternalFile(dst);
            if (null == from || null == to) {
                throw new ScriptException("incorrect file name of from /to");
            }
            copyFile(from, to);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static PrintStream openAsPrinter(String path, boolean isAppend) {
        return openFileAsPrinter(getInternalFile(path), isAppend);
    }
}
