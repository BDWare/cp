package org.bdware.sc.boundry.utils;

import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.doip.audit.EndpointConfig;
import org.bdware.doip.audit.client.AuditIrpClient;
import org.bdware.doip.audit.config.TempConfigStorage;
import org.bdware.irp.stateinfo.StateInfoBase;
import org.bdware.sc.compiler.PermissionStub;
import org.bdware.sc.engine.JSONTool;
import org.bdware.sc.node.Permission;
import wrp.jdk.nashorn.api.scripting.ScriptObjectMirror;


@PermissionStub(permission = Permission.IRP)
public class IRPUtil {
    private static final Logger LOGGER = LogManager.getLogger(IRPUtil.class);
    private final AuditIrpClient auditIrpClient;

    private IRPUtil(String config) {
        TempConfigStorage configStorage = new TempConfigStorage(config);
        JsonObject jo = configStorage.load();
        EndpointConfig endpointConfig = configStorage.loadAsEndpointConfig();

        if (jo.has("clientDoId")) {
            this.auditIrpClient =
                    new AuditIrpClient(jo.get("clientDoId").getAsString(), endpointConfig);
        } else {
            this.auditIrpClient = new AuditIrpClient(endpointConfig);
        }
    }

    public static IRPUtil createClient(String jsonObject) {
        return new IRPUtil(jsonObject);
    }

    public String test(String doi) {
        return "create DOClient And hello " + doi + " World";
    }

    public Object resolve(String doId) {
        try {
            StateInfoBase result = auditIrpClient.resolve(doId);
            JsonObject r = new JsonObject();
            r.addProperty("code", 0);
            r.add("handleValues", result.handleValues);
            return JSONTool.convertJsonElementToMirror(result.handleValues);
        } catch (Exception e) {
            JsonObject r = new JsonObject();
            r.addProperty("code", 1);
            r.addProperty("msg", e.getMessage());
            return JSONTool.convertJsonElementToMirror(r);
        }
    }

    public Object register(ScriptObjectMirror mirror) {
        JsonObject jo = JSONTool.convertMirrorToJson(mirror).getAsJsonObject();
        StateInfoBase base = new StateInfoBase();
        JsonObject result = new JsonObject();
        if (jo.has("identifier"))
            base.identifier = jo.get("identifier").getAsString();
        base.handleValues = jo.get("handleValues").getAsJsonObject();
        try {
            String val = auditIrpClient.register(base);
            result.addProperty("code", 0);
            result.addProperty("doId", val);
            if (val == null || val.length() == 0) {
                result.addProperty("code", 1);
                result.addProperty("msg", auditIrpClient.getLastResponse().getResponseMessage());
            }
            return JSONTool.convertJsonElementToMirror(result);
        } catch (Exception e) {
            result.addProperty("code", 1);
            result.addProperty("msg", e.getMessage());
            return JSONTool.convertJsonElementToMirror(result);
        }
    }
}
