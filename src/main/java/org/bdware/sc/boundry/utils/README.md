# 简介

这个包里对yjs层提供了XXUtil功能的调用。 要添加一个Util需要以下步骤。

## 1.新增加一个带@PermissionStub的注解

注意注解里的Permission要和类名一致。

```java

@PermissionStub(permission = "ABC")
public class ABCUtil {

}
``` 

## 2.在org.bdware.sc.node.Permission中添加这个权限

## 3.在doc项目的@Permission注解说明里
