package org.bdware.sc.compiler;

import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;
import org.bdware.sc.node.InterfaceNode;

public abstract class AnnotationProcessor {
    public void processContract(AnnotationNode anno, ContractNode contractNode) {
        return;
    }

    public void processFunction(AnnotationNode anno, ContractNode contractNode,
            FunctionNode functionNode) throws Exception {
        return;
    }

    public void processInterface(AnnotationNode anno, ContractNode contractNode,
            InterfaceNode functionNode) throws Exception {
        return;
    }
}
