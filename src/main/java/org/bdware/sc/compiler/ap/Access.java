package org.bdware.sc.compiler.ap;

import org.bdware.sc.compiler.AnnotationProcessor;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;

public class Access extends AnnotationProcessor {
    @Override
    public void processContract(AnnotationNode anno, ContractNode contractNode) {
        for (FunctionNode fn : contractNode.getFunctions()) {
            fn.addAnnotation(anno);
        }
    }

}
