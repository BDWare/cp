package org.bdware.sc.compiler.ap;

import org.bdware.sc.compiler.AnnotationProcessor;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.CostDetail;
import org.bdware.sc.node.FunctionNode;
import org.bdware.sc.util.JsonUtil;

public class Cost extends AnnotationProcessor {
    @Override
    public void processFunction(AnnotationNode anno, ContractNode contractNode,
            FunctionNode functionNode) {
        CostDetail detail = JsonUtil.fromJson(anno.getArgs().get(0), CostDetail.class);
        functionNode.setCost(detail);
        if (detail.isCountGas())
            contractNode.setInstrumentBranch(true);
    }
}
