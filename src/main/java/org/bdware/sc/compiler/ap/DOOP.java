package org.bdware.sc.compiler.ap;

import org.bdware.sc.bean.DoipOperationInfo;
import org.bdware.sc.compiler.AnnotationProcessor;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;
import org.bdware.sc.node.InterfaceNode;


// DOOP is designed for DoipModule which contains specific functions for RepositoryHandler
public class DOOP extends AnnotationProcessor {
    @Override
    public void processFunction(AnnotationNode anno, ContractNode contractNode,
            FunctionNode functionNode) throws Exception {
        // 通过DOOP注解，解析对应的值，并放进对应的FunctionNode中
        functionNode.setIsExport(true);
        functionNode.setIsDoipOperation(true);
        functionNode.setDoipOperationInfo(DoipOperationInfo.create(anno, contractNode));
    }

    @Override
    public void processInterface(AnnotationNode anno, ContractNode contractNode,
            InterfaceNode interfaceNode) throws Exception {
        interfaceNode.setIsDoipOperation(true);
        interfaceNode.setDoipOperationInfo(DoipOperationInfo.create(anno, contractNode));
    }
}
