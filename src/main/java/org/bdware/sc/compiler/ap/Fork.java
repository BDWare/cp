package org.bdware.sc.compiler.ap;

import org.bdware.sc.bean.ForkInfo;
import org.bdware.sc.compiler.AnnotationProcessor;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;
import org.bdware.sc.node.InterfaceNode;

public class Fork extends AnnotationProcessor {
    @Override
    public void processFunction(AnnotationNode anno, ContractNode contractNode,
            FunctionNode functionNode) {
        functionNode.setForkInfo(ForkInfo.create(anno, contractNode));
    }

    @Override
    public void processInterface(AnnotationNode anno, ContractNode contractNode,
            InterfaceNode interfaceNode) {
        interfaceNode.setForkInfo(ForkInfo.create(anno, contractNode));
    }
}
