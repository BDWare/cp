package org.bdware.sc.compiler.ap;

import com.google.gson.JsonParser;
import org.bdware.sc.compiler.AnnotationProcessor;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;

public class HomomorphicDecrypt extends AnnotationProcessor {
    @Override
    public void processFunction(AnnotationNode anno, ContractNode contractNode,
            FunctionNode functionNode) {
        functionNode.setHomomorphicDecrypt(true);
        functionNode.setKeyManagerID(anno.getArgs().get(0));
        functionNode.setSecretID(anno.getArgs().get(1));
        functionNode.setHomoDecryptConf(JsonParser.parseString(anno.getArgs().get(2)));
    }
}

