package org.bdware.sc.compiler.ap;

import org.bdware.sc.bean.JoinInfo;
import org.bdware.sc.compiler.AnnotationProcessor;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;
import org.bdware.sc.node.InterfaceNode;

public class Join extends AnnotationProcessor {
    @Override
    public void processFunction(AnnotationNode anno, ContractNode contractNode,
            FunctionNode functionNode) {
        // functionNode.setRouteInfo(RouteInfo.create(anno,contractNode));
        // 增加标记，在ContractNode中记录Join相关的函数和Join规则
        functionNode.setJoinInfo(JoinInfo.create(anno, contractNode));
    }

    @Override
    public void processInterface(AnnotationNode anno, ContractNode contractNode,
            InterfaceNode interfaceNode) {
        interfaceNode.setJoinInfo(JoinInfo.create(anno, contractNode));
    }
}
