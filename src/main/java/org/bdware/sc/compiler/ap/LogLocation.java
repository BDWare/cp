package org.bdware.sc.compiler.ap;

import org.bdware.sc.compiler.AnnotationProcessor;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;

public class LogLocation extends AnnotationProcessor {
    @Override
    public void processContract(AnnotationNode anno, ContractNode contractNode) {
        for (FunctionNode fn : contractNode.getFunctions()) {
            fn.addAnnotation(anno);
        }
    }

    @Override
    public void processFunction(AnnotationNode anno, ContractNode contractNode,
            FunctionNode functionNode) {
        if (anno != null && anno.getArgs() != null)
            for (String s : anno.getArgs()) {
                if (s.equals("\"dataware\"") || s.equals("\"bdledger\"")
                        || s.equals("\"bdledger:\"")) {
                    functionNode.setLogToBDContract(true);
                } else if (s.startsWith("\"bdledger:") && s.length() > 11) {
                    functionNode.setLogToNamedLedger(true);
                    String[] tmp = s.substring(1, s.length() - 1).split(":");
                    functionNode.addLedgerName(tmp[1]);
                }
            }
    }
}
