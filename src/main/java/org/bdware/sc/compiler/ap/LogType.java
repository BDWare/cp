package org.bdware.sc.compiler.ap;

import org.bdware.sc.compiler.AnnotationProcessor;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;

public class LogType extends AnnotationProcessor {
    @Override
    public void processContract(AnnotationNode anno, ContractNode contractNode) {
        contractNode.setLogType(anno.getArgs());
        for (FunctionNode fn : contractNode.getFunctions()) {
            fn.addAnnotation(anno);
        }
    }

    @Override
    public void processFunction(AnnotationNode anno, ContractNode contractNode,
            FunctionNode functionNode) {
        for (String str : anno.getArgs()) {
            org.bdware.sc.node.LogType type = org.bdware.sc.node.LogType.parse(str);
            functionNode.addLogType(type);
            if (type == org.bdware.sc.node.LogType.Branch)
                contractNode.setInstrumentBranch(true);
        }
    }
}
