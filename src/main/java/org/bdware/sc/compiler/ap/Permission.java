package org.bdware.sc.compiler.ap;

import org.bdware.sc.compiler.AnnotationProcessor;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;

public class Permission extends AnnotationProcessor {
    @Override
    public void processContract(AnnotationNode anno, ContractNode contractNode) {
        contractNode.setPermission(anno.getArgs());
    }
}
