package org.bdware.sc.compiler.ap;

import org.bdware.sc.bean.RouteInfo;
import org.bdware.sc.compiler.AnnotationProcessor;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;
import org.bdware.sc.node.InterfaceNode;

public class Route extends AnnotationProcessor {
    @Override
    public void processFunction(AnnotationNode anno, ContractNode contractNode,
            FunctionNode functionNode) {
        functionNode.setRouteInfo(RouteInfo.create(anno, contractNode));
    }

    @Override
    public void processInterface(AnnotationNode anno, ContractNode contractNode,
            InterfaceNode interfaceNode) {
        interfaceNode.setRouteInfo(RouteInfo.create(anno, contractNode));
    }
}
