package org.bdware.sc.crdt.planning;

public class PlanningTest {
    public static void main(String[] args) {

        final int nodeIdsCount = 1000;
        final int writerCount = 500;
        final int readerCount = 500;
        String[] nodeIds = new String[nodeIdsCount];
        int[] writers = new int[writerCount];
        int[] readers = new int[readerCount];
        for (int i = 0; i < nodeIdsCount; ++i) {
            nodeIds[i] = "node_" + i;
            if (i < writerCount) {
                writers[i] = i;
            }
            if (i >= nodeIdsCount - readerCount) {
                readers[i - nodeIdsCount + readerCount] = i;
            }
        }
        long maxDelay = 300;
        int bandwidthUpload = 30;
        int bandwidthDownload = 30;
        int datasize = 10;
        double domainSize = 100 * datasize;

        PlanningWith0Expansivity planning0 = new PlanningWith0Expansivity(nodeIds, writers, readers,
                maxDelay, bandwidthDownload, bandwidthUpload, datasize);
        PlanningWithkExpansivity planningK = new PlanningWithkExpansivity(nodeIds, writers, readers,
                maxDelay, bandwidthDownload, bandwidthUpload, datasize, domainSize);
        PlanningWith1Expansivity planning1 = new PlanningWith1Expansivity(nodeIds, writers, readers,
                maxDelay, bandwidthDownload, bandwidthUpload, datasize);

        long start = System.currentTimeMillis();
        planning0.adjustAndCalc();
        planning0.allocate();
        long end = System.currentTimeMillis();
        System.out.println("took " + (end - start));
        System.out.println();

        start = System.currentTimeMillis();
        planningK.adjustAndCalc();
        planningK.allocate();
        end = System.currentTimeMillis();
        System.out.println("took " + (end - start));
        System.out.println();

        start = System.currentTimeMillis();
        planning1.adjustAndCalc();
        planning1.allocate();
        end = System.currentTimeMillis();
        System.out.println("took " + (end - start));
    }
}
