package org.bdware.sc.crdt.proxy;

import org.bdware.crdt.set.AWORSet;
import org.bdware.sc.crdt.SharableVarState;

import java.util.Set;

public class AWORSetProxy extends SharableVar<AWORSet<Object>> {
    public AWORSetProxy(String varId, String cpId, SharableVarState.SharableVarConfiguration conf) {
        super(varId, cpId, conf);
    }

    public void add(Object val) {
        if (writerVar != null) {
            AWORSet<Object> delta = writerVar.add(val);
            writerVarDeltaQueue.add(delta);
        }
    }

    public void remove(Object val) {
        if (writerVar != null) {
            AWORSet<Object> delta = writerVar.remove(val);
            writerVarDeltaQueue.add(delta);
        }
    }

    public Set<Object> read() {
        if (readerVar != null) {
            return readerVar.read();
        }
        return writerVar.read();
    }

    @Override
    protected AWORSet<Object> createDeltaCrdt(String nodeId, String varId) {
        return new AWORSet<>(nodeId, varId);
    }
}
