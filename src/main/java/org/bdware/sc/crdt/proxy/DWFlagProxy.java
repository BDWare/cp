package org.bdware.sc.crdt.proxy;

import org.bdware.crdt.flag.DWFlag;
import org.bdware.sc.crdt.SharableVarState;

public class DWFlagProxy extends SharableVar<DWFlag> {
    public DWFlagProxy(String varId, String cpId, SharableVarState.SharableVarConfiguration conf) {
        super(varId, cpId, conf);
    }

    public void enable() {
        if (writerVar != null) {
            DWFlag delta = writerVar.enable();
            writerVarDeltaQueue.add(delta);
        }
    }

    public void disable() {
        if (writerVar != null) {
            DWFlag delta = writerVar.disable();
            writerVarDeltaQueue.add(delta);
        }
    }

    public boolean read() {
        if (readerVar != null) {
            return readerVar.read();
        }
        return writerVar.read();
    }

    @Override
    protected DWFlag createDeltaCrdt(String nodeId, String varId) {
        return new DWFlag(nodeId, varId);
    }
}
