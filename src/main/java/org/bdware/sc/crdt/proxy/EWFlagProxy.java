package org.bdware.sc.crdt.proxy;

import org.bdware.crdt.flag.EWFlag;
import org.bdware.sc.crdt.SharableVarState;

public class EWFlagProxy extends SharableVar<EWFlag> {
    public EWFlagProxy(String varId, String cpId, SharableVarState.SharableVarConfiguration conf) {
        super(varId, cpId, conf);
    }

    public void enable() {
        if (writerVar != null) {
            EWFlag delta = writerVar.enable();
            writerVarDeltaQueue.add(delta);
        }
    }

    public void disable() {
        if (writerVar != null) {
            EWFlag delta = writerVar.disable();
            writerVarDeltaQueue.add(delta);
        }
    }

    public boolean read() {
        if (readerVar != null) {
            return readerVar.read();
        }
        return writerVar.read();
    }

    @Override
    protected EWFlag createDeltaCrdt(String nodeId, String varId) {
        return new EWFlag(nodeId, varId);
    }
}
