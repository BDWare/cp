package org.bdware.sc.crdt.proxy;

import org.bdware.crdt.counter.GCounter;
import org.bdware.sc.crdt.SharableVarState;

import java.util.Map;

public class GCounterProxy extends SharableVar<GCounter> {

    public GCounterProxy(String varId, String cpId,
            SharableVarState.SharableVarConfiguration conf) {
        super(varId, cpId, conf);
    }

    public void inc() {
        if (writerVar != null) {
            GCounter delta = writerVar.inc();
            writerVarDeltaQueue.add(delta);
        }
    }

    public void inc(long var) {
        if (writerVar != null) {
            GCounter delta = writerVar.inc(var);
            writerVarDeltaQueue.add(delta);
        }
    }

    public Long read() {
        if (readerVar != null) {
            return readerVar.read();
        }
        return writerVar.read();
    }

    public Map<String, Long> getM() {
        if (readerVar != null) {
            return readerVar.getM();
        }
        return writerVar.getM();
    }

    @Override
    protected GCounter createDeltaCrdt(String nodeId, String varId) {
        return new GCounter(nodeId, varId);
    }
}
