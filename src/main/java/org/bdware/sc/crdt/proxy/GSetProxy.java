package org.bdware.sc.crdt.proxy;

import org.bdware.crdt.set.GSet;
import org.bdware.sc.crdt.SharableVarState;

import java.util.Set;

public class GSetProxy extends SharableVar<GSet<Object>> {
    public GSetProxy(String varId, String cpId, SharableVarState.SharableVarConfiguration conf) {
        super(varId, cpId, conf);
    }

    public void add(Object val) {
        if (writerVar != null) {
            GSet<Object> delta = writerVar.add(val);
            writerVarDeltaQueue.add(delta);
        }
    }

    public Set<Object> read() {
        if (readerVar != null) {
            return readerVar.read();
        }
        return writerVar.read();
    }

    @Override
    protected GSet<Object> createDeltaCrdt(String nodeId, String varId) {
        return new GSet<>(nodeId, varId);
    }
}
