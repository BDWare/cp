package org.bdware.sc.crdt.proxy;

import org.bdware.crdt.register.LWWRegister;
import org.bdware.sc.crdt.SharableVarState;

public class LWWRegisterProxy extends SharableVar<LWWRegister<Long, Object>> {
    public LWWRegisterProxy(String varId, String cpId,
            SharableVarState.SharableVarConfiguration conf) {
        super(varId, cpId, conf);
    }

    public void write(Object val) {
        if (writerVar != null) {
            LWWRegister<Long, Object> delta = writerVar.write(System.currentTimeMillis(), val);
            writerVarDeltaQueue.add(delta);
        }
    }

    public Object read() {
        if (readerVar != null) {
            return readerVar.read();
        }
        return writerVar.read();
    }

    @Override
    protected LWWRegister<Long, Object> createDeltaCrdt(String nodeId, String varId) {
        return new LWWRegister<>(nodeId, varId);
    }
}
