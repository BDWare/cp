package org.bdware.sc.crdt.proxy;

import org.bdware.crdt.register.MVRegister;
import org.bdware.sc.crdt.SharableVarState;

public class MVRegisterProxy extends SharableVar<MVRegister<Object>> {

    public MVRegisterProxy(String varId, String cpId,
            SharableVarState.SharableVarConfiguration conf) {
        super(varId, cpId, conf);
    }

    public void write(Object val) {
        if (writerVar != null) {
            MVRegister<Object> delta = writerVar.write(val);
            writerVarDeltaQueue.add(delta);
        }
    }

    public void resolve(Object val) {
        MVRegister<Object> delta = writerVar.resolve();
        writerVarDeltaQueue.add(delta);
    }

    public Object read() {
        if (readerVar != null) {
            return readerVar.read();
        }
        return writerVar.read();
    }

    @Override
    protected MVRegister<Object> createDeltaCrdt(String nodeId, String varId) {
        return new MVRegister<>(nodeId, varId);
    }
}
