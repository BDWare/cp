package org.bdware.sc.crdt.proxy;

import org.bdware.crdt.counter.PNCounter;
import org.bdware.sc.crdt.SharableVarState;

public class PNCounterProxy extends SharableVar<PNCounter> {
    public PNCounterProxy(String varId, String cpId,
            SharableVarState.SharableVarConfiguration conf) {
        super(varId, cpId, conf);
    }

    public void inc() {
        if (writerVar != null) {
            PNCounter delta = writerVar.inc();
            writerVarDeltaQueue.add(delta);
        }
    }

    public void inc(long val) {
        if (writerVar != null) {
            PNCounter delta = writerVar.inc(val);
            writerVarDeltaQueue.add(delta);
        }
    }

    public void dec() {
        if (writerVar != null) {
            PNCounter delta = writerVar.dec();
            writerVarDeltaQueue.add(delta);
        }
    }

    public void dec(long val) {
        if (writerVar != null) {
            PNCounter delta = writerVar.dec(val);
            writerVarDeltaQueue.add(delta);
        }
    }

    public Long read() {
        if (readerVar != null) {
            return readerVar.read();
        }
        return writerVar.read();
    }

    @Override
    protected PNCounter createDeltaCrdt(String nodeId, String varId) {
        return new PNCounter(nodeId, varId);
    }
}
