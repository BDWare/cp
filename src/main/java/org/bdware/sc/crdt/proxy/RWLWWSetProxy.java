package org.bdware.sc.crdt.proxy;

import org.bdware.crdt.set.RWLWWSet;
import org.bdware.sc.crdt.SharableVarState;

import java.util.Set;

public class RWLWWSetProxy extends SharableVar<RWLWWSet<Long, Object>> {
    public RWLWWSetProxy(String varId, String cpId,
            SharableVarState.SharableVarConfiguration conf) {
        super(varId, cpId, conf);
    }

    public void add(Object val) {
        if (writerVar != null) {
            RWLWWSet<Long, Object> delta = writerVar.add(System.currentTimeMillis(), val);
            writerVarDeltaQueue.add(delta);
        }
    }

    public void remove(Object val) {
        if (writerVar != null) {
            RWLWWSet<Long, Object> delta = writerVar.remove(System.currentTimeMillis(), val);
            writerVarDeltaQueue.add(delta);
        }
    }

    public Set<Object> read() {
        if (readerVar != null) {
            return readerVar.read();
        }
        return writerVar.read();
    }

    @Override
    protected RWLWWSet<Long, Object> createDeltaCrdt(String nodeId, String varId) {
        return new RWLWWSet<>(nodeId, varId);
    }
}
