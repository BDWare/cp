package org.bdware.sc.engine;

import wrp.jdk.nashorn.api.scripting.NashornScriptEngineFactory;

import javax.script.ScriptEngine;

public class TestClassFilter {
    public TestClassFilter() {
        final String script = "print(java.lang.System.getProperty(\"java.home\"));"
                + "print(\"Create file variable\");" + "var File = Java.type(\"java.io.File\");";
        NashornScriptEngineFactory factory = new NashornScriptEngineFactory();
        ScriptEngine engine = factory.getScriptEngine(new YJSFilter());
        try {
            engine.eval(script);
            System.out.print("test push in");
        } catch (Exception e) {
            System.out.println("exception caught:" + e.toString());
        }
    }
}
