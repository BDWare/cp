package org.bdware.sc.engine.hook;

import org.bdware.mockjava.MockUtil;
import org.bdware.sc.JSEngine;
import org.bdware.sc.bean.ContractRequest;
import org.bdware.sc.bean.ProjectConfig;
import org.bdware.sc.engine.DesktopEngine;
import org.bdware.sc.node.AnnotationHook;
import org.bdware.sc.node.ArgPacks;

public class MockTemplateHandler implements AnnotationHook {
    @Override
    public ArgPacks handle(JSEngine engine, ArgPacks argPacks) {
        try {
            ContractRequest request = argPacks.request;
            if (request.fromDebug()) {
                System.out.println(request.getAction());
                DesktopEngine desktopEngine = (DesktopEngine) engine;
                ProjectConfig projectConfig = desktopEngine.getProjectConfig();
                String template = projectConfig.getMock(request.getAction());
                if (template != null && template.length() > 0) {
                    System.out.println(template);
                    MockUtil Mock = new MockUtil();
                    argPacks.ret = Mock.mock(template).toString();
                    return argPacks;
                } else
                    return argPacks; // When mock config is null defined just ignore.
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return argPacks;
        }
    }
}
