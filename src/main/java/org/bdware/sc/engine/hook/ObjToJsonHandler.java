package org.bdware.sc.engine.hook;

import org.bdware.doip.codec.doipMessage.DoipMessage;
import org.bdware.sc.JSEngine;
import org.bdware.sc.engine.JSONTool;
import org.bdware.sc.node.AnnotationHook;
import org.bdware.sc.node.ArgPacks;

public class ObjToJsonHandler implements AnnotationHook {
    @Override
    public ArgPacks handle(JSEngine desktopEngine, ArgPacks argPacks) {
        if (argPacks.ret instanceof DoipMessage)
            return argPacks;
        argPacks.ret = JSONTool.convertMirrorToJson(argPacks.ret);
        return argPacks;
    }
}
