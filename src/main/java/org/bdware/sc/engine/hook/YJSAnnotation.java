package org.bdware.sc.engine.hook;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface YJSAnnotation {
    String name() default "";
}
