package org.bdware.sc.entity;

import org.bdware.doip.codec.JsonDoipMessage;
import org.bdware.doip.codec.doipMessage.DoipMessage;
import org.bdware.sc.engine.JSONTool;
import org.bdware.sc.util.JsonUtil;

public class DoipMessagePacker {
    // the DoipMessagePacker is raised by http/doip
    public String source;
    // the well-composed DoipMessage
    public DoipMessage rawDoipMsg;

    public DoipMessagePacker() {}

    public DoipMessagePacker(String source, DoipMessage rawMsg) {
        this.source = source;
        this.rawDoipMsg = rawMsg;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Object toJSObject() {
        return JSONTool.convertJsonElementToMirror(
                JsonUtil.parseObject(JsonDoipMessage.fromDoipMessage(rawDoipMsg)));
    }
}
