package org.bdware.sc.handler;

public class Exitor implements Runnable {
    public void run() {
        System.out.println("ContractHandler: exit in 2 seconds!");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
