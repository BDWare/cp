package org.bdware.sc.memory;

import java.io.Serializable;

public class JSEDump implements Serializable {
    long invokeID;
    long ranSeed;
    int numsOfCopies;

    public JSEDump(long id, long ra, int nums) {
        invokeID = id;
        ranSeed = ra;
        numsOfCopies = nums;
    }

    public void printContent() {
        System.out.println("invokeID=" + invokeID);
        System.out.println("ranSeed=" + ranSeed);
        System.out.println("numsOfCopies=" + numsOfCopies);
    }
}
