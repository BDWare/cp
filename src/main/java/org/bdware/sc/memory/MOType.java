package org.bdware.sc.memory;

import jdk.internal.dynalink.beans.StaticClass;
import wrp.jdk.nashorn.api.scripting.ScriptObjectMirror;

public enum MOType {
    String(true), Int(true), Double(true), Boolean(true), JSObject(false), JSArray(
            false), JavaObject(
                    false), Method(false), Undefined(true), JSFunction(false), JSStatic(false);

    private boolean isPrimitive;

    MOType(boolean isPrimitive) {
        this.isPrimitive = (isPrimitive);
    }

    public static MOType getType(Object obj) {
        if (obj == null)
            return Undefined;
        if (obj instanceof Integer) {
            return Int;
        } else if (obj instanceof Double) {
            return Double;
        } else if (obj instanceof String) {
            return String;
        } else if (obj instanceof ScriptObjectMirror) {
            // ------
            return JSObject;
        } else if (obj instanceof StaticClass) {
            return JSStatic;
        } else if (obj instanceof Boolean) {
            return Boolean;
        }
        return JSObject;
    }

    public boolean isPrimitive() {
        return isPrimitive;
    }

}
