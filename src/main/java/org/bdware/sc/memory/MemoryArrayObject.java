package org.bdware.sc.memory;

public class MemoryArrayObject extends MemoryJSObject {

    private static final long serialVersionUID = -5805776423219733634L;

    public MemoryArrayObject(long id) {
        super(id);
        type = MOType.JSArray;
    }

}
