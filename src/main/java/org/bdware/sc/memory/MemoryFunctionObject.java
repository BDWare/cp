package org.bdware.sc.memory;

import java.util.HashMap;
import java.util.Map;

public class MemoryFunctionObject extends MemoryObject {
    private static final long serialVersionUID = 5169037078273981613L;
    Map<String, Long> fields;

    public MemoryFunctionObject(long id) {
        super(id);
        fields = new HashMap<>();
        type = MOType.JSFunction;
    }

    public void addField(String key, long id) {
        fields.put(key, id);
    }
}
