package org.bdware.sc.memory;

import java.util.LinkedHashMap;

public class MemoryJSObject extends MemoryObject {
    private static final long serialVersionUID = -2290414347562477503L;
    LinkedHashMap<String, Long> fields;

    public MemoryJSObject(long id) {
        super(id);
        fields = new LinkedHashMap<>();
        type = MOType.JSObject;
    }

    public void addField(String key, long id) {
        fields.put(key, id);
    }
}
