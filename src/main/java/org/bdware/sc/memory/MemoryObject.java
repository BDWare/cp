package org.bdware.sc.memory;

import org.bdware.sc.util.JsonUtil;

import java.io.Serializable;

public class MemoryObject implements Serializable {

    private static final long serialVersionUID = -7830175031856452056L;
    public long id;
    public MOType type;
    Object data;

    public MemoryObject(long id) {
        this.id = id;
    }

    public String toString() {
        return JsonUtil.toJson(this);
    }
}
