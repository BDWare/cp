package org.bdware.sc.syncMech;

public class EstimateUtil {
    public static final String DUMP_TIME = "dumpTime";
    public static final String DUMP_TIMES = "dumpTimes";
    long dumpTime; // 用时
    long dumpTimes; // 次数
    long dumpFile; // 状态大小，dumpFile的次数也是dumpTimes

    public static final String LOADMEMORY_TIME = "loadMemoryTime";
    public static final String LOADMEMORY_TIMES = "loadMemoryTimes";
    long loadMemoryTime;
    long loadMemoryTimes;

    public static final String EXECUTE_TIME = "executeTime";
    public static final String EXECUTE_TIMES = "executeTimes";
    long executeTime;
    long executeTimes;

    public static final String REDO_TRANS_FILE = "redoTransFile";
    public static final String REDO_TRANS_TIMES = "redoTransTimes";
    long redoTransFIle;
    long redoTransTimes;
}
