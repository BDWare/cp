package org.bdware.sc.syncMech;

import java.io.Serializable;

public class SyncRecord implements Serializable {
    SyncType type;
    String fileName; // eg:/memory/2020-...

    public SyncRecord(SyncType t) {
        this.type = t;
    }

    public SyncRecord(SyncType t, String str) {
        type = t;
        fileName = str;
    }

    public void setType(SyncType ty) {
        this.type = ty;
    }

    public SyncType getType() {
        return this.type;
    }

    public void setFileName(String file) {
        this.fileName = file;
    }

    public String getFileName() {
        return this.fileName;
    }

    public String getContent() {
        StringBuilder str = new StringBuilder();
        str.append(type.toString() + ";" + fileName);
        return str.toString();
    }

    public static SyncRecord loadFromString(String str) {
        String[] strs = str.split(";");
        return new SyncRecord(SyncType.convert(strs[0]), strs[1]);
    }
}
