package org.bdware.sc.syncMech;

import java.io.Serializable;

public enum SyncType implements Serializable {
    Memory, Trace, Trans;

    public static SyncType convert(String str) {
        switch (str) {
            case "Memory":
                return Memory;
            case "Trace":
                return Trace;
            case "Trans":
                return Trans;
            default:
                return null;
        }
    }
}
