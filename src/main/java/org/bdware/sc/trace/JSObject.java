package org.bdware.sc.trace;

import java.io.Serializable;

/*
 * js中对象 ScriptObject JO
 */
public class JSObject extends JSScript implements Serializable {

    public JSObject(int id) {
        this.objID = id;
    }
}
