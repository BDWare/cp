package org.bdware.sc.trace;

import java.io.Serializable;

public class TraceDone extends Trace implements Serializable {
    int id;

    public TraceDone(int id) {
        this.id = id;
    }

    public int getID() {
        return id;
    }

    @Override
    public String traceContent() {
        return "[TraceDone] allocID:" + id + "\n";
    }
}
