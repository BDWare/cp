package org.bdware.sc.trace;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * 数组创建时有初始值，进行初始化
 */
public class TraceInitArray extends Trace implements Serializable {
    int arrayId; // 被初始化的数组id
    List<Object> keys = new ArrayList<Object>(); // 数组下标可能是整数，对象名等
    List<Object> values = new ArrayList<Object>(); // 数组内内容

    public TraceInitArray(int id) {
        this.arrayId = id;
        keys = new ArrayList<Object>();
        values = new ArrayList<Object>();
    }

    public void put(Object key, Object value) {
        keys.add(key);
        values.add(value);
    }

    public int getLength() {
        return values.size();
    }

    public int getArrayId() {
        return this.arrayId;
    }

    public Object getKey(int i) {
        return keys.get(i);
    }

    public Object getValue(int i) {
        return values.get(i);
    }

    @Override
    public String traceContent() {
        StringBuilder str = new StringBuilder();
        str.append("[TraceInitArray]\n");
        str.append("ArrayID=" + arrayId + "\n");
        for (int i = 0; i < values.size(); i++)
            str.append("key=" + keys.get(i) + ";value=" + values.get(i) + "\n");
        return str.toString();
    }
}
