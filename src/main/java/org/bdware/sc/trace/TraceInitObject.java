package org.bdware.sc.trace;

import java.io.Serializable;

public class TraceInitObject extends Trace implements Serializable {
    int id;
    int id2;

    public TraceInitObject(int id, int id2) {
        this.id = id;
        this.id2 = id2;
    }

    public int getId() {
        return id;
    }

    public int getId2() {
        return id2;
    }

    @Override
    public String traceContent() {
        StringBuilder str = new StringBuilder();
        str.append("[TraceInitObject]\n");
        str.append("id=" + id + " ; id2=" + id2 + "\n");
        return str.toString();
    }
}
