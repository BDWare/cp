package org.bdware.sc.trace;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TraceRecord implements Serializable {
    private static final long serialVersionUID = 34643133713102276L;

    public List<Trace> traces;

    public TraceRecord() {
        traces = new ArrayList<Trace>();
    }


    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < traces.size(); i++) {
            // str.append("com/yancloud/sc/trace" + i + ":\n");
            str.append(traces.get(i).traceContent() + "\n");
        }
        return str.toString();
    }


    /*
     * 加入一条新的trace
     */
    public void record(Trace trace) {
        traces.add(trace);
    }

    public int length() {
        return traces.size();
    }
}
