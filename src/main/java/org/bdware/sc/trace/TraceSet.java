package org.bdware.sc.trace;

import java.io.Serializable;

/*
 * 记录nashorn中setElem和setProp的trace 自定义对象和数组的set不同
 */
public class TraceSet extends Trace implements Serializable {
    int owner; // 该对象的id
    Object key;
    Object value;

    public TraceSet(int id, Object k, Object v) {
        owner = id;
        key = k;
        value = v;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public void setKey(Object k) {
        this.key = k;
    }

    public void setValue(Object v) {
        this.value = v;
    }

    public int getOwner() {
        return this.owner;
    }

    public Object getKey() {
        return this.key;
    }

    public Object getValue() {
        return this.value;
    }

    @Override
    public String traceContent() {
        StringBuilder str = new StringBuilder();
        str.append("[TraceSet]\n");
        str.append("owner=" + owner + "\n");
        str.append("key=" + key.getClass() + " " + key.toString() + "\n");
        str.append("value=" + value.getClass() + " " + value.toString() + "\n");
        return str.toString();
    }
}
