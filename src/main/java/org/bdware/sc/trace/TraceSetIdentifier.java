package org.bdware.sc.trace;

public class TraceSetIdentifier {
    int owner;
    Object key;

    public TraceSetIdentifier(int id, Object k) {
        this.owner = id;
        this.key = k;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null && this == null)
            return true;
        else if (obj == null && this != null)
            return false;
        else if (obj != null && this == null)
            return false;

        if (this == obj)
            return true;

        TraceSetIdentifier obj2 = (TraceSetIdentifier) obj;

        if (this.owner != obj2.owner)
            return false;

        if (this.key instanceof String && obj2.key instanceof String && obj2.key.equals(this.key))
            return true;
        else if (this.key instanceof Double && obj2.key instanceof Double
                && obj2.key.equals(this.key))
            return true;
        else
            System.out.println("[TraceSetIdentifier] error : encounter new key type : "
                    + this.key.getClass().getName());

        return false;
    }

    @Override
    public int hashCode() {
        int result = 17, temp = 0;
        result = 31 * result + owner;

        if (key instanceof String) {
            String k = (String) key;
            result = result * 31 + k.hashCode();
            result = result * 31 + temp;
        } else if (key instanceof Double) {
            double t = (Double) key;
            temp = (int) t;
            result = result * 31 + temp;
        } else {
            System.out.println("[TraceSetIdentifier] error : encounter new key type : "
                    + this.key.getClass().getName());
        }

        if (result < 0)
            result = -result;

        return result;
    }
}
