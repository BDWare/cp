package org.bdware.sc.trace;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import wrp.jdk.nashorn.internal.runtime.Property;

/*
 * 记录nashorn中ScriptObject创建的trace
 */
public class TraceSetup extends Trace implements Serializable {
    JSScript obj; // 被创建的ScriptObject
    List<Property> properties;

    public TraceSetup(JSScript o) {
        this.obj = o;
        properties = new ArrayList<Property>();
    }

    public JSScript getObj() {
        return this.obj;
    }

    public void add(Property p) {
        properties.add(p);
    }

    public Property get(int i) {
        return properties.get(i);
    }

    public int proLength() {
        return properties.size();
    }

    @Override
    public String traceContent() {
        StringBuilder str = new StringBuilder();
        str.append("[TraceSetup]\n");
        if (obj instanceof JSObject)
            str.append(((JSObject) obj).getObjID() + "," + ((JSObject) obj).getClass() + " "
                    + /* map.toString() + */ "\n");
        else if (obj instanceof JSArray)
            str.append(((JSArray) obj).getObjID() + "," + ((JSArray) obj).getClass() + " "
                    + /* map.toString() + */"\n");

        if (properties.size() > 0) {
            str.append("properties : " + "\n");
            for (int i = 0; i < properties.size(); i++)
                str.append(properties.get(i) + "\n");
        }

        return str.toString();
    }
}
