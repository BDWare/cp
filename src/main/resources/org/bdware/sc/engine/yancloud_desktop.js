var YancloudUtil = org.bdware.sc.boundry.JavaScriptEntry;
var DOMUtil = org.bdware.sc.boundry.utils.DOMUtil;
var LedgerUtil = org.bdware.sc.boundry.utils.LedgerUtil;
var Global = {};
var Indexes = {};
Indexes.timeIndex = org.bdware.sc.boundry.TimeIndex;
Global.version = "V0.90_20200510";
var defineProp = function (key, val) {
    Global[key] = val;
}
var executeContract = function (contractid, action, arg) {
    return YancloudUtil.executeContract(contractid, action, arg);
}

var executeContractByDOI = function (contractDOI, action, arg) {
    return YancloudUtil.executeContractByDOI(contractDOI, action, arg);
}

var getAuthInfo = function () {
    return YancloudUtil.getAuthInfo();
}

var setAuthInfo = function (authInfo) {
    return YancloudUtil.setAuthInfo(authInfo);
}