package maskingJobs;

import org.junit.Test;

import java.io.*;

public class DataXTest {
    @Test
    public void dataXHomeTest() throws IOException {
        MaskingJob mj = new MaskingJob();
        StringBuffer content = new StringBuffer();
        String s = null;
        File directory = new File("");
        String path = directory.getCanonicalPath();
        try {
            BufferedReader bf = new BufferedReader(
                    new FileReader("./src/test/data-mask/maskingJobs/config.json"));
            while ((s = bf.readLine()) != null) {
                content.append(s.trim());
            }
            s = content.toString();
        } catch (Exception e) {
        }
        String ans = mj.getMaskedData(s).getAsString();
        System.out.println(ans);
    }
}
