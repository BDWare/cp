package org.bdware.analysis.dynamic.test;

import org.bdware.sc.analysis.dynamic.FSAnalysis;
import org.bdware.sc.bean.Contract;
import org.bdware.sc.util.FileUtil;

public class FSAnalysisTest {
    public static void main(String[] args) throws Exception {
        Contract contract = new Contract();
        FSAnalysis.isDebug = true;
        String content = FileUtil.getFileContent(
                "/Users/hulingxuan/git/SmartContract/contractExamples/fsanlysis.yjs");
        contract.setScript(null == content ? "" : content);
        FSAnalysis.staticVerify(contract);
    }
}
