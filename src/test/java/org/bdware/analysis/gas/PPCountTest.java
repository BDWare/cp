package org.bdware.analysis.gas;

import org.bdware.analysis.BasicBlock;
import org.bdware.analysis.CFGraph;
import org.bdware.sc.compiler.YJSCompiler;
import org.bdware.sc.engine.DesktopEngine;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

public class PPCountTest extends PPCount {
    public PPCountTest(CFGraph cfg, int flag) {
        super(cfg, flag);
    }

    public static void main(String[] args) throws Exception {
        String path = "/Users/hulingxuan/git/SmartContract/output/main.yjs";

        ContractNode contractNode = null;
        YJSCompiler compiler = new YJSCompiler();
        contractNode = compiler.compile(new FileInputStream(path), null);
        DesktopEngine engine = new DesktopEngine();
        engine.loadContract(null, contractNode, false);

        Map<String, byte[]> clzs = engine.dumpClass();
        Map<String, MethodNode> methods = new HashMap<>();
        for (byte[] clz : clzs.values()) {
            ClassNode classNode = new ClassNode();
            ClassReader cr = new ClassReader(clz);
            cr.accept(classNode, ClassReader.EXPAND_FRAMES);
            for (MethodNode mn : classNode.methods) {
                methods.put(mn.name, mn);
            }
        }
        int flag = 0;
        for (FunctionNode fn : contractNode.getFunctions()) {
            functionList.add(fn.functionName);
            // flag++;
            MethodNode mn = methods.get("log");
            if (mn != null) {
                CFGraph cfg = new CFGraph(mn) {
                    @Override
                    public BasicBlock getBasicBlock(int id) {
                        return new BasicBlock(id);
                    }
                };
                cfg.getLabelOrder();
                PPCount countFee = new PPCount(cfg, flag);

                // countFee.analysis();
                // cfg.printSelf();
            }
        }

        System.out.println(callFunction);
        System.out.println(branchCount);
        System.out.println(BlockInsn);
        System.out.println(ppMap);
        Evaluates feEvaluates = new Evaluates();
        System.out.println(feEvaluates.getCallFee());

        // System.out.println(callFunction);
        // System.out.println(branchCount);
        // System.out.println(BlockInsn);
        // System.out.println(ppMap);
        // Evaluates feEvaluates = new Evaluates(callFunction);
        // System.out.println(feEvaluates.getCallFee());
    }
}
