package org.bdware.analysis.gas;

import org.bdware.sc.compiler.YJSCompiler;
import org.bdware.sc.engine.DesktopEngine;
import org.bdware.sc.node.ContractNode;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String[] args) throws Exception {
        String path = "/Users/hulingxuan/git/SmartContract/output/main.yjs";
        String tracePath = "/Users/hulingxuan/git/SmartContract/output/main.trace";
        ContractNode contractNode = null;
        YJSCompiler compiler = new YJSCompiler();
        contractNode = compiler.compile(new FileInputStream(path), null);
        DesktopEngine engine = new DesktopEngine();
        engine.loadContract(null, contractNode, false);
        Map<String, byte[]> clzs = engine.dumpClass();
        Map<String, MethodNode> methods = new HashMap<>();
        for (byte[] clz : clzs.values()) {
            ClassNode classNode = new ClassNode();
            ClassReader cr = new ClassReader(clz);
            cr.accept(classNode, ClassReader.EXPAND_FRAMES);
            for (MethodNode mn : classNode.methods) {
                methods.put(mn.name, mn);
            }
        }
        // for (FunctionNode fn : contractNode.getFunctions()) {
        // funNameList.add(fn.functionName);
        // }
        // CollectAnalysis collection = null;
        // for (FunctionNode fn : contractNode.getFunctions()) {
        // MethodNode mn = methods.get(fn.functionName);
        // if (mn != null) {
        // TaintResult.nLocals = mn.maxLocals;
        // TaintResult.nStack = mn.maxStack;
        // TaintCFG cfg = new TaintCFG(mn);
        // // cfg.printSelf();
        // TracedFile tf = new TracedFile(new FileInputStream(tracePath));
        // cfg.getLabelOrder();
        // collection = new CollectAnalysis(cfg, tf);
        // collection.analysis();
        // }
        // }
        // System.out.println(CollectAnalysis.branchCount);
        // System.out.println(varInsnCount);
        // System.out.println(callFunction);
        // Evaluates forecast = new Evaluates(callFunction, varInsnCount, branchCount, "log");
        // System.out.println(forecast.getSum());
    }
}
