package org.bdware.sc;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class ByteArrayTest {
    @Test
    public void abc() {
        String str = "abc6787689dd";
        byte[] data = str.getBytes(StandardCharsets.UTF_8);
        String hexStr = ByteUtils.toHexString(data);
        System.out.println("hashCode:" + Arrays.hashCode(data));
        System.out.println("hexStr:" + hexStr);
        // hashCode:-606029994
        // hexStr:616263363738373638396464
    }

    @Test
    public void abc2() {
        String str = "abc678768229dd";
        byte[] data = str.getBytes(StandardCharsets.UTF_8);
        String hexStr = ByteUtils.toHexString(data);
        System.out.println("hashCode:" + Arrays.hashCode(data));
        System.out.println("hexStr:" + hexStr);
        // hashCode:1712735702
        // hexStr:6162633637383736383232396464
        //
    }
}
