package org.bdware.sc;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.bdware.sc.bean.Contract;
import org.bdware.sc.engine.JSONTool;
import org.bdware.sc.util.JsonUtil;
import org.junit.Test;

public class ExecuteFunctionWithoutLimitTest {
    @Test
    public void go() {
        String arg =
                "{\"funcName\":\"getWriteCandidates\",\"funcArgs\":[\"041016acfbc8f4068a24d38a99a1355449ba958f7e905d66617cec9eda311e3001f8d15a4440b6eb61d7b7bd94bb434b30a518623c1593540e7d32eb72fe8088e1\",[\"04a68e0f34cd28484f67cf4e108eb64b8d565d18ecd916e9049151f97d1553872bf8706b3be9e418b7e6136e627d8e81c82a7ce3fd7045ed5382ae3196aa3ac418\",\"041016acfbc8f4068a24d38a99a1355449ba958f7e905d66617cec9eda311e3001f8d15a4440b6eb61d7b7bd94bb434b30a518623c1593540e7d32eb72fe8088e1\",\"04cd228cc3eeec97d912a5158eb25edfcb37947f9934a7645a3311eeeb06f1af05f155f2019ff1edc716d3d10a79a67714284a06ef16d61ccea80aa6816110d157\"],3,{\"doId\":\"abc\",\"body\":\"abcdefg\"}]}";

        JsonObject body = JsonUtil.parseString(arg).getAsJsonObject();
        String funcName = body.get("funcName").getAsString();
        JsonArray arr = body.getAsJsonArray("funcArgs");
        Object[] funcArgs = JsonUtil.fromJson(arr, Object[].class);
        System.out.println(funcArgs);
    }

    @Test
    public void go2() {
        ContractProcess instance = new ContractProcess(124, "bac");
        Contract c = new Contract();
        c.setScript("contract abc{}");
        instance.setContract(c);
        String arg =
                "{\"funcName\":\"getWriteCandidates\",\"funcArgs\":[\"041016acfbc8f4068a24d38a99a1355449ba958f7e905d66617cec9eda311e3001f8d15a4440b6eb61d7b7bd94bb434b30a518623c1593540e7d32eb72fe8088e1\",[\"04a68e0f34cd28484f67cf4e108eb64b8d565d18ecd916e9049151f97d1553872bf8706b3be9e418b7e6136e627d8e81c82a7ce3fd7045ed5382ae3196aa3ac418\",\"041016acfbc8f4068a24d38a99a1355449ba958f7e905d66617cec9eda311e3001f8d15a4440b6eb61d7b7bd94bb434b30a518623c1593540e7d32eb72fe8088e1\",\"04cd228cc3eeec97d912a5158eb25edfcb37947f9934a7645a3311eeeb06f1af05f155f2019ff1edc716d3d10a79a67714284a06ef16d61ccea80aa6816110d157\"],3,{\"doId\":\"abc\",\"body\":\"abcdefg\"}]}";
        JsonObject body = JsonUtil.parseString(arg).getAsJsonObject();
        String funcName = body.get("funcName").getAsString();
        JsonArray arr = body.getAsJsonArray("funcArgs");
        Object[] funcArgs = new Object[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            funcArgs[i] = JSONTool.convertJsonElementToMirror(arr.get(i));
        }
        System.out.println(funcArgs);

    }
}
