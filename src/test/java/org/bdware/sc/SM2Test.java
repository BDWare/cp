package org.bdware.sc;

import org.junit.Test;
import org.zz.gmhelper.SM2KeyPair;

import java.math.BigInteger;

public class SM2Test {
    @Test
    public void run() {
        String str =
                "{publicKey:\"04398dfde44290595cd098cd2f904b36367c69f9011719d43fb0955f823cf1386764769bc7c0a5649dcb316d552998a5c106afd268d9db8b6482ce527544a7bd15\",privateKey:\"82a119ee46db52778182682f11e21980b6de3070b070a2f58e614af66775d6fc\"}";
        String pubKey =
                "04398dfde44290595cd098cd2f904b36367c69f9011719d43fb0955f823cf1386764769bc7c0a5649dcb316d552998a5c106afd268d9db8b6482ce527544a7bd15";
        // 04398dfde44290595cd098cd2f904b36367c69f9011719d43fb0955f823cf1386764769bc7cOa5649dcb316d552998a5c106afd268d9db8b6482ce527544a7bd15

        String privKey = "82a119ee46db52778182682f11e21980b6de3070b070a2f58e614af66775d6fc";
        // 82a119ee46db52778182682f11e21980b6de3070b070a2f58e614af66775d6fc
        SM2KeyPair key = new SM2KeyPair(SM2KeyPair.publicKeyStr2ECPoint(pubKey),
                new BigInteger(privKey, 16));
        System.out.println("hello");
    }
}
