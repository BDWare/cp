package org.bdware.sc.boundry.test;

import org.bdware.sc.boundry.AccountIndex;
import org.bdware.sc.boundry.JavaScriptEntry;
import org.bdware.sc.boundry.utils.AESUtil;
import org.bdware.sc.engine.DesktopEngine;
import org.junit.Test;
import wrp.jdk.nashorn.api.scripting.ScriptObjectMirror;
import wrp.jdk.nashorn.internal.objects.Global;
import wrp.jdk.nashorn.internal.runtime.Context;
import wrp.jdk.nashorn.internal.runtime.ScriptObject;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class ACIndexTest {
    public static void main(String[] args) {
        AccountIndex index = AccountIndex.createIndex();
        DesktopEngine engine = new DesktopEngine();
        engine.getDesktopGlobal();
        Context.setGlobal(JavaScriptEntry.getEngineGlobal());
        ScriptObjectMirror mir = (ScriptObjectMirror) ScriptObjectMirror
                .wrap(Global.allocate(new int[0]), engine.getDesktopGlobal());
        mir.setMember("account", "ac1");
        mir.setMember("file", "time");
        mir.setMember("dataLength", "32");
        ScriptObject obj = index.createFile(mir);
        System.out.println(obj);
        mir.clear();
        for (int i = 0; i < 10000000; i++) {
            mir.setMember("account", "ac1");
            mir.setMember("file", "time");
            mir.setMember("content",
                    "61c1dc1d49ccd251b2f73567c776b4607dd36b12fa64e49e7ca53630102d0389");
            mir.setMember("date", 1578042000 + i);
            obj = index.manullyIndex(mir);
        }
        long start = System.currentTimeMillis();
        mir.clear();
        mir.setMember("account", "ac1");
        mir.setMember("file", "time");
        mir.setMember("startTime", 1578042000);
        mir.setMember("endTime", 1578042060);
        obj = index.requestByTime(mir);
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void test() throws NoSuchMethodException {
        Method abc = AESUtil.class.getMethod("encrypt", String.class, String.class);
        Parameter[] param = abc.getParameters();
        System.out.println(param[0].getName());
    }
}
