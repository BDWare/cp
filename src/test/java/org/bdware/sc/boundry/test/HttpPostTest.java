package org.bdware.sc.boundry.test;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class HttpPostTest {
    public static String querySection(String str) throws Exception {
        str = "http://162.105.138.123/net25/" + str;
        URL url = new URL(str);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        InputStream input = connection.getInputStream();
        Scanner sc = new Scanner(input, "GB2312");
        StringBuilder sb = new StringBuilder();
        while (sc.hasNextLine()) {
            sb.append(sc.nextLine()).append("\n");
        }
        sc.close();
        return sb.toString();
    }

    private static String buildProcess(ProcessBuilder builder) throws IOException {
        // new String(text.getBytes("ISO-8859-1"),"GBK")
        Process process = builder.start();
        Scanner scanner = new Scanner(process.getInputStream(), "ISO-8859-1");
        StringBuilder stringBuilder = new StringBuilder();
        while (scanner.hasNextLine()) {
            stringBuilder.append(scanner.nextLine()).append("\n");
        }
        return new String(stringBuilder.toString().getBytes(StandardCharsets.ISO_8859_1), "GB2312");
    }

    public static String get24Html(String url) {
        ProcessBuilder builder = new ProcessBuilder("curl", url, "-H", "Connection: keep-alive",
                "-H", "Cache-Control: max-age=0", "-H", "Origin: http://162.105.138.123", "-H",
                "Upgrade-Insecure-Requests: 1", "-H",
                "Content-Type: application/x-www-form-urlencoded", "-H",
                "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                "-H",
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                "-H", "Referer: http://162.105.138.123/net25/query.asp", "-H",
                "Accept-Encoding: gzip, deflate", "-H", "Accept-Language: zh-CN,zh;q=0.9", "-H",
                "Cookie: setstring=%CB%CE%CC%E5%2FBLACK%2F4%2F%C1%A5%CA%E9%2FBLUE%2F2%2F%CB%CE%CC%E5%2Fgreen%2F3%2F%C1%A5%CA%E9%2FBLUE%2F2; clientadd=10%2E1%2E12%2E68; serveradd=162%2E105%2E138%2E123; ASPSESSIONIDAQQTRBDT=HHKDBDKBHGLMACLALGIFGKPO",
                "--compressed");
        try {
            return buildProcess(builder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "failed";
    }

    public static String getCatalogHtml(String key) {
        ProcessBuilder builder = new ProcessBuilder("curl",
                "http://162.105.138.123/net25/readns-25.htm", "-H", "Connection: keep-alive", "-H",
                "Cache-Control: max-age=0", "-H", "Origin: http://162.105.138.123", "-H",
                "Upgrade-Insecure-Requests: 1", "-H",
                "Content-Type: application/x-www-form-urlencoded", "-H",
                "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                "-H",
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                "-H", "Referer: http://162.105.138.123/net25/query.asp", "-H",
                "Accept-Encoding: gzip, deflate", "-H", "Accept-Language: zh-CN,zh;q=0.9", "-H",
                "Cookie: setstring=%CB%CE%CC%E5%2FBLACK%2F4%2F%C1%A5%CA%E9%2FBLUE%2F2%2F%CB%CE%CC%E5%2Fgreen%2F3%2F%C1%A5%CA%E9%2FBLUE%2F2; clientadd=10%2E1%2E12%2E68; serveradd=162%2E105%2E138%2E123; ASPSESSIONIDAQQTRBDT=HHKDBDKBHGLMACLALGIFGKPO",
                "--compressed");
        try {
            return buildProcess(builder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "failed";
    }

    public static String query24(String keyword) {
        ProcessBuilder builder = new ProcessBuilder("curl",
                "http://162.105.138.123/net25/readwhole.asp", "-H", "Connection: keep-alive", "-H",
                "Cache-Control: max-age=0", "-H", "Origin: http://162.105.138.123", "-H",
                "Upgrade-Insecure-Requests: 1", "-H",
                "Content-Type: application/x-www-form-urlencoded", "-H",
                "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                "-H",
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                "-H", "Referer: http://162.105.138.123/net25/query.asp", "-H",
                "Accept-Encoding: gzip, deflate", "-H", "Accept-Language: zh-CN,zh;q=0.9", "-H",
                "Cookie: setstring=%CB%CE%CC%E5%2FBLACK%2F4%2F%C1%A5%CA%E9%2FBLUE%2F2%2F%CB%CE%CC%E5%2Fgreen%2F3%2F%C1%A5%CA%E9%2FBLUE%2F2; clientadd=10%2E1%2E12%2E68; serveradd=162%2E105%2E138%2E123; ASPSESSIONIDAQQTRBDT=HHKDBDKBHGLMACLALGIFGKPO",
                "--data",
                "query=_keyword&B1=%BC%EC+%CB%F7&colid=2.0&colid=2.1&colid=2.14&colid=2.25&colid=2.34&colid=2.65&colid=2.136&colid=2.138"
                        .replace("_keyword", keyword),
                "--compressed");
        try {
            return buildProcess(builder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "failed";
    }

    public static void main(String[] args) throws Exception {
        // String data = getCatalogHtml(null);
        // FileOutputStream fout = new FileOutputStream("./output/shiji.txt");
        // Document doc = Jsoup.parse(data);
        // Element body = doc.select("body").get(0);
        // for (int i = 0; i < body.children().size(); i++) {
        // Element ele = body.child(i);
        //
        // if (ele.tagName().equals("span"))
        // System.out.println(ele.text());
        // }

        System.out.println(querySection("aa"));
    }

    static class Item {
        String title;
        String url;
    }
}
