package org.bdware.sc.boundry.utils.test;

import org.bdware.sc.boundry.utils.AESUtil;
import wrp.jdk.nashorn.internal.runtime.ScriptObject;

public class AESUtilTest {
    public static void main(String[] args) throws Exception {
        String skey = AESUtil.generateKey(256);
        System.out.println(skey);
        String hw = "Hello World!";
        ScriptObject so = AESUtil.encrypt(skey, hw);
        String iv = so.get("iv").toString();
        String cipherText = so.get("cipherText").toString();
        System.out.println(iv);
        System.out.println(cipherText);
        String result = AESUtil.decrypt(skey, cipherText, iv);
        System.out.println(result);
    }
}
