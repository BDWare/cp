package org.bdware.sc.boundry.utils.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.sc.boundry.utils.DOIPUtil;
import org.junit.Test;

public class DOIPUtilTest {
    static Logger LOGGER = LogManager.getLogger(DOIPUtilTest.class);

    @Test
    public void testRetrieve() {
        DOIPUtil util = DOIPUtil.createClient("tcp://127.0.0.1:21032");
        LOGGER.info(util.retrieve("AnnotationExample", "{\"operation\":\"main\",\"arg\":\"\"}"));

    }
}
