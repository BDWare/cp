package org.bdware.sc.boundry.utils.test;

import org.bdware.bdledger.api.grpc.Client;
import org.bdware.bdledger.api.grpc.pb.CommonProto;
import org.bdware.bdledger.api.grpc.pb.LedgerProto;
import org.bdware.sc.conn.ByteUtil;
import org.bdware.sc.util.HashUtil;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import org.junit.Test;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class LedgerUtilTest {
    public static void main(String[] arg) {
        String ip = "39.104.202.92";
        ip = "39.104.205.122";
        ip = "39.104.201.40";
        Client c = new Client(ip, 21121);
        System.out.println(c.clientVersionSync().getVersion());
        System.out.println(c.getLedgersSync().toString());
        String from = "0xb60e8dd61c5d32be8058bb8eb970870f07233155";
        LedgerProto.SendTransactionResponse ret =
                c.sendTransactionSync("bdcontract", CommonProto.TransactionType.MESSAGE, from,
                        System.currentTimeMillis(), from, "hello".getBytes(StandardCharsets.UTF_8));
        System.out.println("=====" + HashUtil.byteArray2Str(ret.getHash().toByteArray()));
    }

    @Test
    public void transToB64() {
        String str = "1dc589951b10840e097c793764f8ad63ff577ef7";
        str = "3c011ab510c5756db9aab1f2ac1a14742b85200f";
        str = "867020d3463126c0f5ea41967100865770ca1873";
        str = "3f40a6afcd4e7c7db1ee8a3cf54074be270c6847";
        str = "ab7d541b4f320f77fe424082d79d1f4ca2a40f84";
        str = "246f5527c3182d162ea8f1c3f5e0be05d9269517";
        str = "d148e40be1078c707891ba12ed270978d81dad30";
        str = "dbda443da8a6da3b4703b06250f07f7df3e04d72";
        str = "d16da370021447c1c1136f97f9975069b1f22ddb";
        str = "448b314de358384a55b9c5d2eae7596cff4e3587";
        str = "a114aa22365c2d61ee1c242c755d82c035783e41";
        str = "d9be17b4287ed9a548901ef0e738f65f25dc7041";
        str = "e90dbc995add64b26fa483b1b0ad7747b19ad579";
        byte[] bytes = ByteUtils.fromHexString(str);
        String hash = ByteUtil.encodeBASE64(bytes);
        System.out.println(URLEncoder.encode(hash));
    }

    @Test
    public void readData() {
        String hash = "kNkTGrOLKlMiVHiCx/Ik3Tx3DDI=";
        hash = "mEoVJx4k2L5nhKY6exjtJWmU7RA=";
        hash = "OTg0YTE1MjcxZTI0ZDhiZTY3ODRhNjNhN2IxOGVkMjU2OTk0ZWQxMA==";
        byte[] data = ByteUtil.decodeBASE64(hash);
        // <ByteString@67aaeb8d size=20
        // contents="<D\206P!\241\005\017\366\321\312\336\323cWiS>\226\207">
        // System.out.println(new String(data));
    }
}
