package org.bdware.sc.boundry.utils.test;

import java.sql.*;

public class SQLUtilTest {
    public static void main(String[] args) {
        Connection con;
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://39.106.6.6:3306/haiou";
        // MySQL配置时的用户名
        String user = "haiou";
        // MySQL配置时的密码
        String password = "haiou";
        // 遍历查询结果集
        try {
            // 加载驱动程序
            Class.forName(driver);
            // 1.getConnection()方法，连接MySQL数据库！！
            con = DriverManager.getConnection(url, user, password);

            if (!con.isClosed())
                System.out.println("Succeeded connecting to the Database!");
            // 2.创建statement类对象，用来执行SQL语句！！
            Statement statement = con.createStatement();

            String sql = "select * from catering limit 0,10";
            // 3.ResultSet类，用来存放获取的结果集！！
            ResultSet rs = statement.executeQuery(sql);
            ResultSetMetaData metaData = rs.getMetaData();
            System.out.println("FetchedSize:" + rs.getFetchSize());
            String job = null;
            String id = null;

            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                // int columnType = metaData.getColumnType(i);
                System.out.println(
                        metaData.getColumnTypeName(i) + " --> " + metaData.getColumnName(i));
            }
            while (rs.next()) {
                // 输出结果
                System.out.println(id + "777\t" + job);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
