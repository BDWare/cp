package org.bdware.sc.engine.hook.test;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.bdware.sc.engine.hook.ArgSchemaVisitor;
import org.junit.Test;

public class ArgSchemaTest {
    @Test
    public void test1() {
        JsonObject realData = JsonParser
                .parseString("{\"data\":[{\"name\":\"zzz\"},{\"name\":\"aaa\"}],\"doId\":1}")
                .getAsJsonObject();
        JsonObject schema =
                JsonParser.parseString("{\"data\":[{\"name\":\"string\"}],\"doId\":\"number\"}")
                        .getAsJsonObject();


        ArgSchemaVisitor visitor = new ArgSchemaVisitor(realData);
        visitor.visit(schema);
        System.out.println(visitor.getStatus());
        System.out.println(visitor.getException().toString());
    }

    @Test
    public void test2() {
        JsonObject realData = JsonParser
                .parseString(
                        "{\"data\":[{\"name\":\"zzz\"},{\"name\":\"aaa\"}],\"doId\":\"string\"}")
                .getAsJsonObject();
        JsonObject schema =
                JsonParser.parseString("{\"data\":[{\"name\":\"string\"}],\"doId\":\"number\"}")
                        .getAsJsonObject();


        ArgSchemaVisitor visitor = new ArgSchemaVisitor(realData);
        visitor.visit(schema);
        System.out.println(visitor.getStatus());
        System.out.println(visitor.getException().toString());
        System.out.println(visitor.errorCode);
    }

    @Test
    public void test3() {
        JsonElement realData = JsonParser.parseString("data.json");
        JsonElement schema = JsonParser.parseString("\"string\"");


        ArgSchemaVisitor visitor = new ArgSchemaVisitor(realData);
        visitor.visit(schema);
        System.out.println(visitor.getStatus());
        System.out.println(visitor.getException().toString());
        System.out.println(visitor.errorCode);
    }

    // 缺少必选项
    @Test
    public void test4() {
        JsonObject realData =
                JsonParser.parseString("{\"data\":[{\"name\":\"zzz\"},{\"name\":\"aaa\"}]}")
                        .getAsJsonObject();
        JsonObject schema =
                JsonParser.parseString("{\"data\":[{\"name\":\"string\"}],\"!doId\":\"string\"}")
                        .getAsJsonObject();
        ArgSchemaVisitor visitor = new ArgSchemaVisitor(realData);
        visitor.visit(schema);
        System.out.println(visitor.getStatus());
        System.out.println(visitor.getException());
        System.out.println(visitor.errorCode);
    }
}
