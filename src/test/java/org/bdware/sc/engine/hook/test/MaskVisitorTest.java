package org.bdware.sc.engine.hook.test;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.bdware.sc.engine.hook.MaskVisitor;

public class MaskVisitorTest {
    public static void main(String[] args) {
        String mask =
                "[{\"score\":\"md5\",\"name\":\"fpe\",\"grade\":\"edp\",\"number\":\"aes\",\"info\":{\"age\":\"edp\",\"sex\":\"md5\"}}]";
        String data =
                "[{\"score\":11,\"name\":\"ccq\",\"grade\":98.5,\"number\":2001210533,\"info\":{\"age\":\"18\",\"sex\":\"m\"}},{\"score\":11,\"name\":\"ccq\",\"grade\":98.5,\"number\":2001210533}]";

        String m1 = "{\"score\":\"edp\",\"name\":\"md5\"}";
        String d1 = "{\"score\":\"95\",\"name\":\"zzz\"}";
        JsonElement je = JsonParser.parseString(data);
        JsonElement maskje = JsonParser.parseString(mask);
        MaskVisitor visitor = new MaskVisitor(je);
        visitor.visit(maskje);
        JsonElement root = visitor.get();
        System.out.println(root.toString());
    }
}
