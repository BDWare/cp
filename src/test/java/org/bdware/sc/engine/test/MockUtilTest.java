package org.bdware.sc.engine.test;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.bdware.mockjava.MockSchemaParser;
import org.bdware.mockjava.MockUtil;

public class MockUtilTest {
    public static void main(String[] args) {
        // "{'result|min-max':1}":min-max之间的一个数字 如："{'result|1-100':1}" 返回: {"result":21}
        // "{'result':'@integer(1,100)'}"; 返回一个整数：{"result":5171830293164278}
        // "{'result':'@string'}" 返回一个字符串 {"result":"2ejKET"}
        // "{'result':'@datetime'}" "{'result':'@date'}" "{'result':'@time'}"
        // "{'result':'@cname'}" "{'result':'@name'}" "{'result':'@first'}" "{'result':'@last'}"
        // "{'result':'@email'}" "{'result':'@ip'}" {'result':'@url'}"
        // "{'result':'@province'}" "{'result':'@city'}" "{'result':'@county'}"
        // {'id':'@integer','email':'@email','password':'@string','name':'@name'}
        // String template="{'result|1-100':1}";
        ///
        // Object res = mock("'@email'");
        // if (res instanceof ScriptObjectMirror)
        // System.out.println(JSONTool.copy((ScriptObjectMirror) res));
        // else System.out.println(res);
        // System.out.println(res);
        // "{'list|1-5':[{'id|+1':1,'data':'@datetime','nickname': '@cname','email':'@email'}]}
        String str = "\"{\\\"score\\\":1}\"";
        str = "{\"status\":\"success\", \"data\":[{\"score\":1, \"list\":[4,5,6]}]}";
        String str1 = "[{\"score\":1}]";
        MockSchemaParser parser = new MockSchemaParser();
        JsonElement ele = JsonParser.parseString(str);
        System.out.println(ele.toString());
        parser.visit(ele);
        JsonElement ele2 = parser.get();
        System.out.println(ele2);
        Object ret = MockUtil.mock(ele2.toString());

        // System.out.println(
        // (JSONTool.copy((jdk.nashorn.api.scripting.ScriptObjectMirror)
        // mock(ele2.toString()))).toString());

        System.out.println(MockUtil.mock("'@string'"));
    }
}
