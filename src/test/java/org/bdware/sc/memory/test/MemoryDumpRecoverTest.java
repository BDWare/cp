package org.bdware.sc.memory.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class MemoryDumpRecoverTest {

    public static void main(String[] args) throws IOException {
        // memoryRecover("AppData");
        // memoryRecover("Test");
    }

    public static String file2Str(String file) {
        StringBuilder sb = new StringBuilder();
        try {
            Scanner sc = new Scanner(new FileInputStream(file));
            while (sc.hasNextLine()) {
                sb.append(sc.nextLine()).append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    /*
     * //仅支持public路径下测试 public static void memoryRecover(String contractName) { String name =
     * contractName + "/" + contractName;
     * 
     * //启动合约 ContractManager cm2 = new ContractManager(); cm2.yjsPath = JudgeStatus.yjsPath;
     * Contract c = new Contract(); c.setType(ContractType.Sole); String id = "169412582";
     * c.setID(id); c.setScript(RedoRecoverTest.file2Str(
     * "/Users/zhangyixuan/Lab/newProject/BDContract/front-agent/BDWareProjectDir/public/" + name +
     * ".yjs")); String key =
     * "{\"publicKey\":\"041d7b4818817736b3d64305675f04e9a00a12ce23c72e6740f83785edcffec6bae2c178fbc934f66d72e26f89a2e7a2c966dc3f0a8e415663c9b933af7a2e9ff8\",\"privateKey\":\"18505919022281880113072981827955639221458448578012075254857346196103069175443\"}";
     * SM2KeyPair pair = SM2KeyPair.fromJson(key); c.doSignature(pair);
     * System.out.println("<<<Start a contract>>> : " + cm2.startContractAndRedirect(c, null));
     * 
     * 
     * 
     * 
     * //loadMemory long time1 = System.currentTimeMillis(); File mem = new
     * File("./cp/recoverTestFiles/memory/" + contractName + ".txt"); String res =
     * cm2.loadMemory(contractName, mem.getAbsolutePath()); long time2 = System.currentTimeMillis();
     * try { Thread.sleep(3000); } catch (InterruptedException e) { // TODO Auto-generated catch
     * block e.printStackTrace(); } System.out.println("<<<Load memory>>> : " + res); double cost =
     * ((double)time2 - (double)time1)/1000; System.out.println("load memory 用时" + cost + "s");
     * 
     * 
     * String content1 = MemoryDumpUtil.getContentFromFile(mem.getAbsolutePath()); String content2 =
     * cm2.dumpContract(contractName, ""); System.out.println("load之后dump : \n" + content2 +
     * "\n\n"); System.out.println(content1.equals(content2)); }
     */
}
