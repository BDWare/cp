package org.bdware.sc.memory.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.sc.memory.MemoryDumpUtil;
import org.bdware.sc.util.HashUtil;
import org.junit.Test;

import java.net.URL;

public class PrintMemory {
    private static final Logger LOGGER = LogManager.getLogger(PrintMemory.class);

    @Test
    public void printBDCoin() {
        printMemory("/memory/BDCoin.ckpt");
    }

    @Test
    public void printCounter() {
        printMemory("/memory/Counter.ckpt");
    }

    private void printMemory(String path) {
        URL resource = this.getClass().getResource(path);
        assert null != resource;
        String content = MemoryDumpUtil.getContentFromFile(resource.getFile());
        assert null != content;
        LOGGER.info(content.length());
        LOGGER.info(HashUtil.sha3(content));
    }
}
