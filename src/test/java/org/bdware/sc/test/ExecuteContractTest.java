package org.bdware.sc.test;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.bdware.sc.boundry.utils.HttpUtil;
import wrp.jdk.nashorn.internal.runtime.ScriptObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class ExecuteContractTest {
    public static ExecutorService executor = Executors.newFixedThreadPool(100);
    static int repeat = 100000;
    static Counter c = new Counter();

    public static void main(String[] args) throws Exception {
        String testDir = "./scripts/test/";
        File f = new File(testDir);
        if (!f.exists())
            f.mkdirs();
        String[] confs = new String[] {"HelloAtMac", "AnalysisAtMac", "TFAtMac", "HelloAtAli",
                "AnalysisAtAli", "TFAtAli"};
        // http://59.110.5.194:8080/
        String[] urls = new String[] {
                "http://127.0.0.1:18000/SCIDE/CMManager?action=executeContract&&contractID=HelloAtMac&operation=get&arg=world",
                "http://127.0.0.1:18000/SCIDE/CMManager?action=executeContract&&contractID=AppDataContractAtMac&operation=connectDBAndQuery&arg=%7B%22type%22%3A%22takeout%22%2C%22detail%22%3A%22overall%22%2C%22district%22%3A%22%E5%AF%86%E4%BA%91%22%7D&requestID=1576408475257_93",
                "http://127.0.0.1:18000/SCIDE/CMManager?action=executeContract&&contractID=ImageMatcherAtMac&operation=testLocal&arg=%2Fimgs%2Fcup.jpg",
                "http://59.110.5.194:8080/SCIDE/CMManager?action=executeContract&&contractID=HelloAt59&operation=get&arg=world",
                "http://59.110.5.194:8080/SCIDE/CMManager?action=executeContract&&contractID=AppDataContractAt59&operation=connectDBAndQuery&arg=%7B%22type%22%3A%22takeout%22%2C%22detail%22%3A%22overall%22%2C%22district%22%3A%22%E5%AF%86%E4%BA%91%22%7D&requestID=1576408475257_93",
                "http://59.110.5.194:8080/SCIDE/CMManager?action=executeContract&&contractID=ImageMatcherAt59&operation=testLocal&arg=%2Fimgs%2Fcup.jpg"};

        // String[] urls = new String[] {
        //
        // "http://127.0.0.1:18000/SCIDE/CMManager?action=executeContract&&contractID=HelloAt59&operation=get&arg=world",
        //
        // "http://127.0.0.1:18000/SCIDE/CMManager?action=executeContract&&contractID=AppDataContractAt59&operation=connectDBAndQuery&arg=%7B%22type%22%3A%22takeout%22%2C%22detail%22%3A%22overall%22%2C%22district%22%3A%22%E5%AF%86%E4%BA%91%22%7D&requestID=1576408475257_93",
        //
        // "http://127.0.0.1:18000/SCIDE/CMManager?action=executeContract&&contractID=ImageMatcherAt59&operation=testLocal&arg=%2Fimgs%2Fcup.jpg",
        //
        // "http://59.110.5.194:8080/SCIDE/CMManager?action=executeContract&&contractID=HelloAtMac&operation=get&arg=world",
        //
        // "http://59.110.5.194:8080/SCIDE/CMManager?action=executeContract&&contractID=AppDataContractAtMac&operation=connectDBAndQuery&arg=%7B%22type%22%3A%22takeout%22%2C%22detail%22%3A%22overall%22%2C%22district%22%3A%22%E5%AF%86%E4%BA%91%22%7D&requestID=1576408475257_93",
        //
        // "http://59.110.5.194:8080/SCIDE/CMManager?action=executeContract&&contractID=ImageMatcherAtMac&operation=testLocal&arg=%2Fimgs%2Fcup.jpg"
        // };

        String output = "./scripts/test/executeContract_cross.txt";
        FileOutputStream fout = new FileOutputStream(output, true);
        for (int i = 0; i < 1; i++) {
            test(confs[i], urls[i], new PrintStream(fout)); // new PrintStream(fout)
        }
        int pre = 0;
        for (;;) {
            Thread.sleep(1000);
            int curr = c.busy.get() + c.success.get();
            System.out.println(new Gson().toJson(c) + " --> " + (curr - pre) + " " + curr);
            pre = curr;
        }
        // fout.close();
        // System.exit(0);

    }

    private static void test(final String conf, final String url, final PrintStream out) {

        for (int i = 0; i < repeat; i++) {
            executor.execute(() -> {
                ScriptObject r = HttpUtil.get(url);

                // System.out.println("http result:" + r.response);
                if (r.get("response").toString().contains("busy"))
                    c.busy.incrementAndGet();
                else
                    c.success.incrementAndGet();
                JsonObject jo =
                        JsonParser.parseString(r.get("response").toString()).getAsJsonObject();
                int exeTime = jo.get("executeTime").getAsInt();
                out.println(conf + "\t" + exeTime);
            });
        }
    }

    static class Counter {
        AtomicInteger busy = new AtomicInteger(0);
        AtomicInteger success = new AtomicInteger(0);
    }
}
