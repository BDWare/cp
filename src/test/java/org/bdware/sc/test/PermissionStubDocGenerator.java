package org.bdware.sc.test;

import org.bdware.sc.compiler.PermissionStub;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class PermissionStubDocGenerator {

    public static void main(String[] args) {
        Class<?> clz = org.bdware.sc.boundry.utils.FileUtil.class;
        System.out.println("load class " + clz.getCanonicalName());
        clz = org.bdware.sc.boundry.utils.LedgerUtil.class;
        System.out.println("load class " + clz.getCanonicalName());
        clz = org.bdware.sc.boundry.utils.HttpUtil.class;
        System.out.println("load class " + clz.getCanonicalName());
        clz = org.bdware.sc.boundry.utils.DOIPUtil.class;
        System.out.println("load class " + clz.getCanonicalName());
        clz = org.bdware.sc.boundry.utils.SQLUtil.class;
        System.out.println("load class " + clz.getCanonicalName());
        clz = org.bdware.sc.boundry.utils.RocksDBUtil.class;
        System.out.println("load class " + clz.getCanonicalName());
        clz = org.bdware.sc.boundry.utils.CMUtil.class;
        System.out.println("load class " + clz.getCanonicalName());
        clz = org.bdware.sc.boundry.utils.SM2Util.class;
        System.out.println("load class " + clz.getCanonicalName());
        clz = org.bdware.sc.boundry.utils.AsyncUtil.class;
        System.out.println("load class " + clz.getCanonicalName());

        Method[] methods = clz.getDeclaredMethods();
        PermissionStub stub = clz.getAnnotation(PermissionStub.class);
        List<Method> methodList = Arrays.asList(methods);
        methodList.sort(Comparator.comparing(Method::getName));
        System.out.println("## " + stub.permission() + "util\n");
        System.out.printf("可以使用@Permission(\"%s\")来引入%s对象。%n", stub.permission(),
                stub.permission() + "util");
        System.out.printf("```\n@Permission(\"%s\")\ncontract %sExample{\n  ...\n}\n```\n%n",
                stub.permission(), stub.permission());

        for (Method m : methods) {
            if (Modifier.isStatic(m.getModifiers())) {
                System.out.println("### " + m.getName() + "\n");
                Parameter[] parameters = m.getParameters();
                System.out.println("\n#### 参数\n");
                System.out.println("| 序号 | 参数 | 说明 |");
                System.out.println("|---|---|---|");
                int i = 1;
                for (Parameter p : parameters) {
                    System.out.printf("| %d | %s | %s |%n", i, p.getName(),
                            p.getType().getCanonicalName());
                    i++;
                }
                System.out.println("\n#### 使用示例\n");
                System.out.println("```javascript");
                System.out
                        .println("var ret = " + stub.permission() + "Util." + m.getName() + "();");
                System.out.println("```");
            }
        }
    }
}
