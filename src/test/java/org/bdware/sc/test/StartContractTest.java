package org.bdware.sc.test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.bdware.sc.boundry.utils.HttpUtil;
import wrp.jdk.nashorn.internal.runtime.ScriptObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class StartContractTest {
    public static void main(String[] args) throws Exception {
        String testDir = "./scripts/test/";
        File f = new File(testDir);
        if (!f.exists())
            f.mkdirs();
        String[] confs = new String[] {"HelloAtMac", "AnalysisAtMac", "TFAtMac", "HelloAtAli",
                "AnalysisAtAli", "TFAtAli"};
        // http://59.110.5.194:8080/
        String[] urls = new String[] {
                "http://127.0.0.1:18000/SCIDE/CMManager?action=startContractBatched&fileList=[A2.yjs]",
                "http://127.0.0.1:18000/SCIDE/CMManager?action=startContractBatched&fileList=[EleAnalysis.yjs]",
                "http://127.0.0.1:18000/SCIDE/CMManager?action=startContract&owner=04266aa345154d8e224990e33dd47a3203dd927d8c4f6420f63fbab063d9ecb8c4d58751e81940f2092dc4609de22a1980f4d8df39730ba0ddd453e3fa3b7b2a6b&requestID=1576403183009&contractid=Hello&script=empty&path=/Tensorflow/imagematch.yjs&signature=815490e34a3c85bcfe54a740f91e8739cfd6e5daa6c73084de758705e754a43bb245c5c23ff9f46c2a99cc0d627d4cf1da583e20073e85455f6cb10c63bef86b",
                "http://59.110.5.194:8080/SCIDE/CMManager?action=startContractBatched&fileList=[A2.yjs]",
                "http://59.110.5.194:8080/SCIDE/CMManager?action=startContractBatched&fileList=[EleAnalysis.yjs]",
                "http://59.110.5.194:8080/SCIDE/CMManager?action=startContract&owner=04266aa345154d8e224990e33dd47a3203dd927d8c4f6420f63fbab063d9ecb8c4d58751e81940f2092dc4609de22a1980f4d8df39730ba0ddd453e3fa3b7b2a6b&requestID=1576403183009&contractid=Hello&script=empty&path=/Tensorflow/imagematch.yjs&signature=815490e34a3c85bcfe54a740f91e8739cfd6e5daa6c73084de758705e754a43bb245c5c23ff9f46c2a99cc0d627d4cf1da583e20073e85455f6cb10c63bef86b"};

        String output = "./scripts/test/startContract.txt";
        FileOutputStream fout = new FileOutputStream(output, true);
        for (int i = 0; i < urls.length; i++) {
            test(confs[i], urls[i], System.out); // new PrintStream(fout)
        }
        fout.close();
        System.exit(0);
    }

    static int repeat = 1;

    private static void test(String conf, String url, PrintStream out) {
        for (int i = 0; i < repeat; i++) {
            ScriptObject result = HttpUtil.get(url);
            System.out.println("http result:" + result.get("response"));
            JsonObject jo =
                    new JsonParser().parse(result.get("response").toString()).getAsJsonObject();
            int exeTime = jo.get("executeTime").getAsInt();
            out.println(conf + "\t" + exeTime);
        }
    }
}
