package org.bdware.sc.test;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.bdware.sc.http.HttpUtil;
import org.junit.Test;

import java.sql.*;

public class setMaskTest {
    @Test
    public void test1() {

        String url = "http://127.0.0.1:21030/SCIDE/CMManager?action=setMask";
        String contractID = "Hello1";
        String operation = "hello1";
        String maskInfo = "\"md5\"";
        maskInfo = "";
        String pubkey =
                "04c111cde30c257eb9d891653e2a25eb15df5239dd9882389e8daeffd4b16f65f30b30688b58d3e2eaf25ce5a529a601e4924581b55cedf78d94d17864b73c51a7";
        System.out.println(url);
        String sign =
                "30440220138c8ae1956f6af19ddb24b9a0fe5cf9cc4f8ecd25537a8b5c4dcd5f30a4f985022026475279687ccdfb112cbcc307e573333d80f45386281def4df54b8b7532ed79";
        // url+="&contractID="+contractID+"&operation="+operation+"&maskInfo="+maskInfo+"&pubkey="+pubkey+"&signature="+sign;
        url += "&contractID=" + contractID + "&operation=" + operation + "&maskInfo=" + maskInfo;
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=setMask&&pubkey=04c111cde30c257eb9d891653e2a25eb15df5239dd9882389e8daeffd4b16f65f30b30688b58d3e2eaf25ce5a529a601e4924581b55cedf78d94d17864b73c51a7&signature=304602210082f33a4cdbf483428c701753eb08ca430f855d52df18547a367fe14ee638761f022100cab7fa189b03248b54f3cb679d7df6c2548f6743256cad6ee772c532bb34bdf9";
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=ping";
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=setMask"+"&contractID="+contractID+"&maskInfo="+maskInfo;
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=ping";
        System.out.println(url);
        System.out.println(HttpUtil.httpGet(url.toString()));
        String resp = HttpUtil.httpGet(url.toString()).get("response").toString();
        System.out.println(resp);
    }

    @Test
    public void testSetMock() {

        String url = "http://127.0.0.1:21030/SCIDE/CMManager?action=setMock";
        String contractID = "Hello1";
        String operation = "hello1";
        String mockInfo = "\"@string\"";
        // String mockInfo="";
        String pubkey =
                "04c111cde30c257eb9d891653e2a25eb15df5239dd9882389e8daeffd4b16f65f30b30688b58d3e2eaf25ce5a529a601e4924581b55cedf78d94d17864b73c51a7";
        System.out.println(url);
        String sign =
                "30440220138c8ae1956f6af19ddb24b9a0fe5cf9cc4f8ecd25537a8b5c4dcd5f30a4f985022026475279687ccdfb112cbcc307e573333d80f45386281def4df54b8b7532ed79";
        // url+="&contractID="+contractID+"&operation="+operation+"&maskInfo="+maskInfo+"&pubkey="+pubkey+"&signature="+sign;
        url += "&contractID=" + contractID + "&operation=" + operation + "&mockInfo=" + mockInfo;
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=setMask&&pubkey=04c111cde30c257eb9d891653e2a25eb15df5239dd9882389e8daeffd4b16f65f30b30688b58d3e2eaf25ce5a529a601e4924581b55cedf78d94d17864b73c51a7&signature=304602210082f33a4cdbf483428c701753eb08ca430f855d52df18547a367fe14ee638761f022100cab7fa189b03248b54f3cb679d7df6c2548f6743256cad6ee772c532bb34bdf9";
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=ping";
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=setMask"+"&contractID="+contractID+"&maskInfo="+maskInfo;
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=ping";
        System.out.println(url);
        System.out.println(HttpUtil.httpGet(url.toString()));
        String resp = HttpUtil.httpGet(url.toString()).get("response").toString();
        System.out.println(resp);
    }

    @Test
    public void testGetMock() {

        String url = "http://127.0.0.1:21030/SCIDE/CMManager?action=getMock";
        String contractID = "Hello1";
        String operation = "hello1";
        String mockInfo = "@string";
        String pubkey =
                "04c111cde30c257eb9d891653e2a25eb15df5239dd9882389e8daeffd4b16f65f30b30688b58d3e2eaf25ce5a529a601e4924581b55cedf78d94d17864b73c51a7";
        System.out.println(url);
        String sign =
                "30440220138c8ae1956f6af19ddb24b9a0fe5cf9cc4f8ecd25537a8b5c4dcd5f30a4f985022026475279687ccdfb112cbcc307e573333d80f45386281def4df54b8b7532ed79";
        // url+="&contractID="+contractID+"&operation="+operation+"&maskInfo="+maskInfo+"&pubkey="+pubkey+"&signature="+sign;
        url += "&contractID=" + contractID + "&operation=" + operation;
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=setMask&&pubkey=04c111cde30c257eb9d891653e2a25eb15df5239dd9882389e8daeffd4b16f65f30b30688b58d3e2eaf25ce5a529a601e4924581b55cedf78d94d17864b73c51a7&signature=304602210082f33a4cdbf483428c701753eb08ca430f855d52df18547a367fe14ee638761f022100cab7fa189b03248b54f3cb679d7df6c2548f6743256cad6ee772c532bb34bdf9";
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=ping";
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=setMask"+"&contractID="+contractID+"&maskInfo="+maskInfo;
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=ping";
        System.out.println(url);
        System.out.println(HttpUtil.httpGet(url.toString()));
        String resp = HttpUtil.httpGet(url.toString()).get("response").toString();
        System.out.println(resp);
    }

    @Test
    public void test3() {

        String url = "http://127.0.0.1:21030/SCIDE/CMManager?action=setMask";
        String contractID = "Hello";
        String operation = "hello3";
        String maskInfo = "";
        String pubkey =
                "04c111cde30c257eb9d891653e2a25eb15df5239dd9882389e8daeffd4b16f65f30b30688b58d3e2eaf25ce5a529a601e4924581b55cedf78d94d17864b73c51a7";
        System.out.println(url);
        String sign =
                "30440220138c8ae1956f6af19ddb24b9a0fe5cf9cc4f8ecd25537a8b5c4dcd5f30a4f985022026475279687ccdfb112cbcc307e573333d80f45386281def4df54b8b7532ed79";
        // url+="&contractID="+contractID+"&operation="+operation+"&maskInfo="+maskInfo+"&pubkey="+pubkey+"&signature="+sign;
        url += "&contractID=" + contractID + "&operation=" + operation + "&maskInfo=" + maskInfo;
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=setMask&&pubkey=04c111cde30c257eb9d891653e2a25eb15df5239dd9882389e8daeffd4b16f65f30b30688b58d3e2eaf25ce5a529a601e4924581b55cedf78d94d17864b73c51a7&signature=304602210082f33a4cdbf483428c701753eb08ca430f855d52df18547a367fe14ee638761f022100cab7fa189b03248b54f3cb679d7df6c2548f6743256cad6ee772c532bb34bdf9";
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=ping";
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=setMask"+"&contractID="+contractID+"&maskInfo="+maskInfo;
        // url="http://127.0.0.1:21030/SCIDE/CMManager?action=ping";
        System.out.println(url);
        System.out.println(HttpUtil.httpGet(url.toString()));
        String resp = HttpUtil.httpGet(url.toString()).get("response").toString();
        System.out.println(resp);
    }

    @Test
    public void test2() {
        String s1 = "{\"score\":\"md5\",\"name\":\"aes\"}";
        JsonElement je1 = JsonParser.parseString("{\"score\":\"md5\",\"name\":\"aes\"}");
        System.out.println(je1);
    }

    @Test
    public void testHangUp() {
        String url = "http://127.0.0.1:21030/SCIDE/CMManager?action=";
        String contractID = "Hello";
        String urlHangUp = url + "hangUpContractProcess&contractID=" + contractID;
        String urlResume = url + "resumeContractProcess&contractID=" + contractID;
        System.out.println(urlHangUp);
        String resp1 = HttpUtil.httpGet(urlHangUp.toString()).get("response").toString();
        System.out.println(resp1);

        // String operation="hello3";
    }

    @Test
    public void testResume() {
        String url = "http://127.0.0.1:21030/SCIDE/CMManager?action=";
        String contractID = "Hello";
        String urlHangUp = url + "hangUpContractProcess&contractID=" + contractID;
        String urlResume = url + "resumeContractProcess&contractID=" + contractID;
        System.out.println(urlResume);
        String resp2 = HttpUtil.httpGet(urlResume.toString()).get("response").toString();
        System.out.println(resp2);

        // String operation="hello3";
    }

    @Test
    public void testMysql() {
        String contractID = "AAA12BMySQL";
        String url = "http://127.0.0.1:21030/SCIDE/CMManager?action=getMask" + "&contractID="
                + contractID + "&requestID=1";

        System.out.println(url);
        String resp = HttpUtil.httpGet(url.toString()).get("response").toString();
        System.out.println(resp);
    }
}
