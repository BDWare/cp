package org.bdware.sc.test.synctest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class RedoRecoverTest {

    public static void main(String[] args) throws IOException {
        transRecover("Z_Test_pub1", "Z_Test_pub1-1");
    }

    public static void transRecover(String contractName, String argsFileName) {
        // //获得之前的状态
        // ObjectInputStream reader;
        // String memory = null;
        // try {
        // FileInputStream fileout = new FileInputStream("./cp/recoverTestFiles/memory/" +
        // contractName + ".txt");
        // GZIPInputStream gzin = new GZIPInputStream(fileout);
        // reader = new ObjectInputStream(gzin);
        // MemoryDump md = new MemoryDump();
        // md.setObjects((Map<Long, MemoryObject>) reader.readObject());;
        // reader.close();
        // memory = new GsonBuilder().setPrettyPrinting().create().toJson(md);
        // } catch (IOException | ClassNotFoundException e) {
        // e.printStackTrace();
        // }
        //
        //
        //
        //
        // //读取文件中内容;
        // int count = 0;
        // try {
        // File file = new File("./cp/recoverTestFiles/affairs/" + contractName + "/" +
        // argsFileName);
        // FileReader fr = new FileReader(file);
        // BufferedReader br = new BufferedReader(fr);
        // String s = null;
        // while((s=br.readLine()) != null) {
        // count++;
        // }
        // } catch (IOException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        //
        //
        //
        // //启动
        // String name = contractName + "/" + contractName;
        // ContractManager cm2 = new ContractManager();
        // cm2.yjsPath = JudgeStatus.yjsPath;
        // Contract c = new Contract();
        // c.setType(ContractType.Sole);
        // String id = "169412583";
        // c.setID(id);
        // c.setScript(MemoryDumpRecoverTest
        // .file2Str("/Users/zhangyixuan/Lab/newProject/BDContract/front-agent/BDWareProjectDir/public/"
        // + name + ".yjs"));
        // String key =
        // "{\"publicKey\":\"041d7b4818817736b3d64305675f04e9a00a12ce23c72e6740f83785edcffec6bae2c178fbc934f66d72e26f89a2e7a2c966dc3f0a8e415663c9b933af7a2e9ff8\",\"privateKey\":\"18505919022281880113072981827955639221458448578012075254857346196103069175443\"}";
        // SM2KeyPair pair = SM2KeyPair.fromJson(key);
        // c.doSignature(pair);
        // System.out.println("Start a contract : " + cm2.startContractAndRedirect(c, System.out));
        //
        //
        // //recover
        // long time1 = System.currentTimeMillis();
        // String traceFile = "./cp/recoverTestFiles/trans/" + contractName + ".trans";
        // cm2.setTransRecords(contractName,traceFile);
        // cm2.recoverFromTransRecord(contractName,(count - 1) + "");
        // long time2 = System.currentTimeMillis();
        // double cost = ((double)time2 - (double)time1)/1000;
        //
        //
        // //对比
        // String memory2 = cm2.dumpContract(contractName,"");
        // System.out.println("<<<After recover dump>>>" + memory2);
        // if(memory.equals(memory2)) {
        // System.out.println("####################\n####################\n####################");
        // System.out.println("Trans count = " + count + " Contract " + contractName + " recover
        // success!");
        // System.out.println("####################\n####################\n####################");
        // }
        // else {
        // System.out.println("!!!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!!!");
        // System.out.println("Trans count = " + count + " Contract " + contractName + " recover
        // failed!");
        // System.out.println("!!!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!!!");
        //
        // }
        // System.out.println("共" + count + "项事务恢复，用时" + cost + "s");
    }



    public static String file2Str(String file) {
        StringBuilder sb = new StringBuilder();
        try {
            Scanner sc = new Scanner(new FileInputStream(file));
            for (; sc.hasNextLine();) {
                sb.append(sc.nextLine()).append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
