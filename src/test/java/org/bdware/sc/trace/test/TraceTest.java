package org.bdware.sc.trace.test;

import org.bdware.sc.trace.TraceSetIdentifier;

import java.util.HashMap;
import java.util.Map;

public class TraceTest {
    public static void main(String[] args) {
        Map<TraceSetIdentifier, String> map = new HashMap<>();

        TraceSetIdentifier id1 = new TraceSetIdentifier(182, "asd");
        TraceSetIdentifier id2 = new TraceSetIdentifier(182, 3.2);
        TraceSetIdentifier id3 = new TraceSetIdentifier(182, "asd");
        TraceSetIdentifier id4 = new TraceSetIdentifier(182, 3.2);

        map.put(id1, "id1");
        map.put(id2, "id2");
        map.put(id3, "id3");
        map.put(id4, "id4");

        for (String v : map.values()) {
            System.out.println(v);
        }
    }
}
